// pages/api/payment-url.js

import WooCommerceAPI from '@woocommerce/woocommerce-rest-api';

export default async (req, res) => {
  const WooCommerce = new WooCommerceAPI({
    url: 'YOUR_STORE_URL',
    consumerKey: 'YOUR_CONSUMER_KEY',
    consumerSecret: 'YOUR_CONSUMER_SECRET',
    wpAPI: true,
    version: 'wc/v3',
  });

  try {
    // Gọi API WooCommerce để lấy đường dẫn thanh toán PayPal
    const response = await WooCommerce.get('payment-url');
    const { paymentUrl } = response.data;

    res.status(200).json({ paymentUrl });
  } catch (error) {
    console.error('Error retrieving payment URL:', error);
    res.status(500).json({ error: 'Failed to retrieve payment URL' });
  }
};
