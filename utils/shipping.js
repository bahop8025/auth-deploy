import WooCommerce from "./apiWoocommerce";

export const getShippingData = async () => {
  const res = await WooCommerce.get("shipping/zones/1/methods")

  const shipping = res.data;
  return {
    shipping: shipping,
  };
}


