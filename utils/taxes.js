import WooCommerce from "./apiWoocommerce";

export const getTaxesData = async () => {
  const res = await WooCommerce.get("taxes")
  const taxes = res.data;
  return {
    taxes: taxes,
  };
}