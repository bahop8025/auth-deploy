import WooCommerce from "./apiWoocommerce";

export const getAttributes = async () => {
  const res = await WooCommerce.get("products/attributes");
  const attributes = res.data;

  return {
    attributes: attributes,
  };
}

export const getAttributesAll = async () => {
  const { attributes } = await getAttributes();

  const attributeIds = attributes.map((attribute) => {
    return {
      attributeId: attribute.id,
      slug: attribute.slug
    };
  });

  const attributeRequests = attributeIds.map(async (q) => {
    const { attributeId, slug } = q;
    const res = await WooCommerce.get(`products/attributes/${attributeId}/terms`);
    const attributeTerms = res.data;
    attributeTerms.forEach((term) => {
      term.parent_id = attributeId;
      term.slug_parent = slug;
    });
    return attributeTerms;
  });

  const attributeResponses = await Promise.all(attributeRequests);

  const attributesTerm = attributeResponses.reduce((result, attributeTerms, index) => {
    const attributeId = attributeIds[index].attributeId;
    result[attributeId] = attributeTerms;
    return result;
  }, {});


  return {
    attributesAll: attributesTerm,
  };
};
