import WooCommerce from "./apiWoocommerce";

export const getDealsData = async (category = "deals") => {
  try {
    const res = await WooCommerce.get("products", {
      category: category,
    });
    const deals = res.data;
    return {
      deals: deals,
    };
  } catch (error) {
    console.error("Error fetching deals data:", error);
    return {
      deals: []
    };
  }
};