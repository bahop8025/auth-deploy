import WooCommerce from "./apiWoocommerce";

export const getBestSellers = async (perPage = 8, sellerCategory) => {

  const params = {
    per_page: perPage,
  }

  let url = 'products?orderby=popularity';
  if (sellerCategory) {
    url += `&category=${sellerCategory}`
  }

  const response = await WooCommerce.get(url, params);
  const productSellers = response.data;

  return {
    productSellers: productSellers
  };
};
