import WooCommerce from "./apiWoocommerce";

export const getProductsData = async (perPage = 12, page = 1, category, search, id, slug, min_price, max_price, orderby) => {

  const params = {
    per_page: perPage,
    page: page,
    search: search,
  };

  let url = 'products';
  if (category) {
    url = `products?category=${category}`;
    if (orderby) {
      url += `&orderby=${orderby}`;
    } else if (min_price && max_price) {
      url += `&min_price=${min_price}&max_price=${max_price}`;
    }
  } else if (search) {
    url = `products?search=${search}`;
  } else if (id && slug) {
    url = `products?attribute_term=${id}&attribute=${slug}`;
    if (orderby) {
      url += `&orderby=${orderby}`;
    } else if (min_price && max_price) {
      url += `&min_price=${min_price}&max_price=${max_price}`;
    }
  } else if (min_price && max_price) {
    url = `products?min_price=${min_price}&max_price=${max_price}`;
    if (orderby) {
      url += `&orderby=${orderby}`;
    } else if (id && slug) {
      url += `&attribute_term=${id}&attribute=${slug}`;
    }
  } else if (orderby) {
    url = `products?orderby=${orderby}`;
    if (min_price && max_price) {
      url += `&min_price=${min_price}&max_price=${max_price}`
    }
  }



  const response = await WooCommerce.get(url, params);

  const products = response.data;
  const totalProducts = response.headers['x-wp-total'];
  const totalPages = response.headers['x-wp-totalpages'];

  return {
    products: products,
    totalProducts: totalProducts,
    totalPages: totalPages,
  };
};

