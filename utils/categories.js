import WooCommerce from "./apiWoocommerce";
export const getChildCategories = async (parentId) => {
  const res = await WooCommerce.get("products/categories", {
    parent: parentId,
  });
  const childCategories = res.data;
  return childCategories;
};

export const getCategoriesData = async (
  perPage = 100,
  page = 1,
  parentId = 0
) => {
  const res = await WooCommerce.get("products/categories", {
    per_page: perPage,
    page: page,
    parent: parentId,
  });
  const categories = res.data;

  for (let i = 0; i < categories.length; i++) {
    const category = categories[i];
    const childCategories = await getChildCategories(category.id);
    if (childCategories.length > 0) {
      category.children = childCategories;
    }
    categories[i] = category;
  }

  return {
    categories: categories,
    totalProducts: res.headers["x-wp-total"],
    totalPages: res.headers["x-wp-totalpages"],
  };
};
