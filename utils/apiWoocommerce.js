import WooCommerceAPI from "@woocommerce/woocommerce-rest-api";

const WooCommerce = new WooCommerceAPI({
  url: process.env.NEXT_PUBLIC_WORDPRESS_SITE_URL,
  consumerKey: "ck_b9bec4e831c03316b14d2e5c8dd3dbe7aa10c5ca",
  consumerSecret: "cs_5196fc57896524d31bccb409d92654cb01a8f9ba",
  wpAPI: true,
  version: "wc/v3",
});

export default WooCommerce;
