import WooCommerce from "./apiWoocommerce";

export const getLocationsData = async () => {
  const res = await WooCommerce.get("shipping/zones/1/locations")
  const locations = res.data;
  return {
    locations: locations,
  };
}
