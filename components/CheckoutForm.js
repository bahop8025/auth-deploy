import { yupResolver } from "@hookform/resolvers/yup";
import { makeStyles } from "@material-ui/core/styles";
import {
  Alert,
  Box,
  Button,
  Grid,
  Snackbar,
  Stack,
  Typography,
} from "@mui/material";
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import MenuItem from "@mui/material/MenuItem";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-toastify";
import * as yup from "yup";
import WooCommerce from "../utils/apiWoocommerce";
import PaymentModes from "./PaymentModes";
import CountrySelection from "./checkout/CountrySelection";
import InputForm from "./checkout/InputForm";
import { AppContextRest } from "./context/ContextRest";

const colorHover = "#40c6ff";
const useStyles = makeStyles({
  checkoutForm: {
    marginTop: "30px",
    display: "flex",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },
  page: {
    marginBottom: "60px",
    minHeight: "200px",
    backgroundImage: `url("/banner_page.png)`,
    backgroundSize: "cover",
  },

  titlePage: {
    display: "flex",
    paddingTop: "75px",
    paddingBottom: "75px",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },

  rightTextPage: {
    paddingTop: "12px",
    float: "right",
    "@media (max-width: 768px)": {
      float: "inherit",
    },
  },

  mainCheckout: {
    marginTop: "30px",
    display: "flex",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },
  contactNumber: {
    marginBottom: "35px",
  },
  billingAddress: {
    marginBottom: "35px",
  },
  shippingAddress: {
    marginBottom: "35px",
  },
  delivery: {
    display: "flex",
    padding: "15px",
    "@media (max-width: 350px)": {
      display: "block",
    },
  },
  payment: {
    marginLeft: "20px",
    "@media (max-width: 768px)": {
      marginLeft: "0px",
    },
  },
  tabListPay: {
    marginBottom: "20px",
  },
  tabPay: {
    border: "1px solid #40c6ff",
    width: "120px",
    height: "50px",
    background: "white",
    margin: "10px",
    cursor: "pointer",
  },
  tabPanePay: {
    margin: "10px",
  },
  number: {
    border: "1px",
    background: "#40c6ff",
    borderRadius: "30px",
    marginRight: "15px",
    paddingRight: "7px",
    paddingLeft: "7px",
    paddingTop: "3px",
    paddingBottom: "3px",
  },
  style: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    background: "#fff",
    border: "2px solid #000",
    alignItems: "center",
    justifyContent: "center",
  },
  formInput: {
    width: "100%",

    height: 45,
    "&:focusVisible": {
      outline: ["none"],
    },
  },
  boxinput: {
    width: "100%",
    paddingRight: "10px",
    paddingLeft: "10px",
  },
  textTile: {
    color: "white",
    fontFamily: "Merriweather",
    fontSize: "50px",
    fontWeight: 700,
  },
  titleText: {
    color: "white",
    fontFamily: "Muli",
    fontWeight: 400,
    fontSize: "14px",
  },
  titleCheckout: {
    marginBottom: "20px",
  },
  cardPrice: {
    color: `${colorHover}`,
    fontSize: "26px",
    fontWeight: "500",
    fontFamily: "Mulish,sans-serif",
  },
  checkout: {
    display: "flex",
    justifyContent: "space-between",
    padding: "10px 0 10px 0",
  },
});

const schema = yup.object({
  first_name: yup.string().required("First Name is a required field"),
  last_name: yup.string().required("Last Name is a required field"),
  address_1: yup.string().required("InputForm is a required field"),
  address_2: yup.string(),
  Company: yup.string(),
  townCity: yup.string().required("City is a required field"),
  stateCounty: yup.string().required("City is a required field"),
  postcode: yup.string().min(5, "Post Code must be at least 6 charecters"),
  email: yup.string().required().email("Email is not valid."),
  phone: yup
    .string()
    .required()
    .matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/, "Phone number is not valid."),
});

export default function CheckoutForm(props) {
  const { taxes, locations } = props;
  const router = useRouter();
  const classes = useStyles();
  const { cart } = useContext(AppContextRest);
  const [total, setTotal] = useState();
  const [paymentMethod, setPaymentMethod] = useState("cod");
  const [selectedLabel, setSelectedLabel] = useState("");
  const [country, setCountry] = useState(locations);
  const [tax, setTax] = useState(taxes);
  const [selectedCountry, setSelectedCountry] = useState(0);
  const [valueCost, setValueCost] = useState();

  const {
    handleSubmit,
    register,
    formState: { errors },
    reset,
    control,
  } = useForm({
    resolver: yupResolver(schema),
  });

  let nameCountry;
  if (typeof selectedCountry === "object") {
    nameCountry = selectedCountry.country;
  } else if (typeof selectedCountry === "string") {
    nameCountry = selectedCountry;
  }

  let valueCountry = 0;
  if (typeof selectedCountry === "string") {
    valueCountry = 0;
  } else if (typeof selectedCountry === "object" && selectedCountry !== null) {
    valueCountry = parseFloat(selectedCountry.rate);
  }

  const subTotal = parseFloat(total);
  const taxAmount = subTotal * (valueCountry / 100);
  const taxValueTax = subTotal + taxAmount + parseFloat(valueCost);

  useEffect(() => {
    setTotal(
      cart.reduce((acc, curr) => acc + Number(curr.price) * curr.quantity, 0)
    );
  }, [cart]);

  useEffect(() => {
    const storedValue = localStorage.getItem("selectedOption");
    if (storedValue) {
      setValueCost(storedValue);
    }
  }, []);

  // useEffect(() => {
  //   const user = localStorage.getItem("user");
  //   if (!user) {
  //     router.push("/myaccount");
  //   }
  // }, [router]);

  const fromSubmit = async (data) => {
    const taxValue = localStorage.getItem("selectedOption");
    try {
      const orderData = {
        payment_method: paymentMethod,
        payment_method_title: selectedLabel,
        set_paid: false,
        billing: {
          first_name: data.first_name,
          last_name: data.last_name,
          address_1: data.address_1,
          address_2: data.address_2,
          city: data.townCity,
          company: data.Company,
          state: data.stateCounty,
          postcode: data.postcode,
          country: nameCountry,
          email: data.email,
          phone: data.phone,
        },
        shipping: {
          first_name: data.first_name,
          last_name: data.last_name,
          address_1: data.address_1,
          address_2: data.address_2,
          city: data.townCity,
          state: data.stateCounty,
          company: data.Company,
          postcode: data.postcode,
          country: nameCountry,
        },
        line_items: [],
        shipping_lines: [
          {
            method_id: "flat_rate",
            method_title: "Flat Rate",
            total: taxValue,
          },
        ],
      };
      const cartItems = JSON.parse(localStorage.getItem("cart"));
      const lineItems = cartItems.map((item) => ({
        product_id: item.id,
        name: item.name,
        quantity: item.quantity,
        price: item.price,
      }));
      orderData.line_items = lineItems;
      const response = await WooCommerce.post("orders", orderData);
      if (response) {
        localStorage.removeItem("cart");
        localStorage.removeItem("totalQuantity");
        localStorage.removeItem("selectedOption");
        reset();
        const orderDetails = JSON.stringify(response.data);
        const urlOrder = `/OrderSuccess?orderId=${encodeURIComponent(
          orderDetails
        )}`;
        router.push(urlOrder);
      } else {
        toast.error("Order failed");
      }
    } catch (error) {
      router.push("/");
      toast.error("Order failed");
    }
  };

  const handleCountryChange = (selectedOption) => {
    const taxForLocation = tax.find((tax) => tax.country === selectedOption);
    if (taxForLocation === undefined) {
      setSelectedCountry(selectedOption);
    } else {
      setSelectedCountry(taxForLocation);
    }
  };

  const handleOnChange = (event) => {
    setPaymentMethod(event.target.value);
  };
  const handleLabelChange = (label) => {
    setSelectedLabel(label);
  };

  return (
    <>
      {cart.length > 0 ? (
        <form
          onSubmit={handleSubmit(fromSubmit)}
          className={classes.checkoutForm}
        >
          <Grid container>
            <Grid item lg={8} md={8} xs={12}>
              <Typography variant="h5" className={classes.titleCheckout}>
                Shipping Details
              </Typography>
              <InputForm
                id="first_name"
                lable="first_name"
                type="text"
                placeholder="firstName"
                register={{ ...register("first_name") }}
                error={errors.first_name?.message}
              />
              <InputForm
                id="last_name"
                lable="last_name"
                type="text"
                placeholder="lastName"
                register={{ ...register("last_name") }}
                error={errors.first_name?.message}
              />
              <InputForm
                id="address_1"
                lable="Street address"
                type="text"
                placeholder="House number and street name"
                register={{ ...register("address_1") }}
                error={errors.address_1?.message}
              />
              <InputForm
                id="address_2"
                lable="Street address 2"
                type="text"
                placeholder="House number and street name"
                register={{ ...register("address_2") }}
                error={errors.address_2?.message}
              />
              <InputForm
                id="Company"
                lable="Company name "
                type="text"
                placeholder="Company name "
                register={{ ...register("Company") }}
                error={errors.Company?.message}
              />
              <Controller
                id="options"
                name="options"
                control={control}
                render={({ field }) => (
                  <CountrySelection
                    {...field}
                    options={country}
                    onChange={handleCountryChange}
                  />
                )}
              />

              <InputForm
                id="townCity"
                lable="Town / City"
                type="text"
                placeholder="Town / City"
                register={{ ...register("townCity") }}
                error={errors.city?.message}
              />
              <InputForm
                id="stateCounty"
                lable="State / County"
                type="text"
                placeholder="State / County"
                register={{ ...register("stateCounty") }}
                error={errors.city?.message}
              />
              <InputForm
                id="postcode"
                lable="postcode"
                type="text"
                placeholder="Post code"
                register={{ ...register("postcode") }}
                error={errors.postcode?.message}
              />
              <InputForm
                id="phone"
                lable="phone"
                type="text"
                placeholder="Phone"
                register={{ ...register("phone") }}
                error={errors.phone?.message}
              />
              <InputForm
                id="email"
                lable="email"
                type="text"
                placeholder="Email"
                register={{ ...register("email") }}
                error={errors.email?.message}
              />
            </Grid>
            <Grid item lg={4} md={4} xs={12}>
              <Box className={classes.payment}>
                <Typography variant="h5">Your Order</Typography>
                {cart?.length &&
                  cart.map((item, index) => (
                    <MenuItem
                      sx={{ marginBottom: "10px" }}
                      key={`index${index}`}
                    >
                      <Image
                        width={50}
                        height={50}
                        style={{ marginRight: 20 }}
                        src={item?.images?.[0].src}
                        alt="Card image cap"
                      />
                      <ListItemText>
                        <p>
                          Name: <strong>{item.name}</strong>
                        </p>
                        <p>
                          Quantity: <strong>{item.quantity}</strong>
                        </p>
                        <p>
                          Price: <strong>${item.price * item.quantity}</strong>
                        </p>
                      </ListItemText>
                    </MenuItem>
                  ))}
                <div className={classes.checkout}>
                  <span>Total</span>
                  <strong>${total}</strong>
                </div>
                <Divider />

                <div className={classes.checkout}>
                  <span variant="div">Tax</span>
                  {selectedCountry && selectedCountry ? (
                    <strong variant="div">${selectedCountry.rate || 0}</strong>
                  ) : (
                    <strong variant="div">$0</strong>
                  )}
                </div>
                <div className={classes.checkout}>
                  <span variant="div">Shipping</span>
                  {valueCost > 0 ? (
                    <strong>${valueCost}</strong>
                  ) : (
                    <strong>$0</strong>
                  )}
                </div>

                <Divider />

                <div className={classes.checkout}>
                  <span> Sub Total:</span>
                  <strong variant="h5" className={classes.cardPrice}>
                    ${taxValueTax && taxValueTax}
                  </strong>
                </div>

                <PaymentModes
                  handleLabelChange={handleLabelChange}
                  paymentMethod={paymentMethod}
                  handleOnChange={handleOnChange}
                />

                <Stack direction="row" spacing={2} sx={{ width: "100%" }}>
                  <Button
                    type="submit"
                    sx={{ width: "100%", backgroundColor: "#40c6ff" }}
                    variant="contained"
                  >
                    Place Order
                  </Button>
                </Stack>

                <Snackbar>
                  <Alert
                    variant="filled"
                    severity="success"
                    sx={{ width: "100%" }}
                  >
                    Order Successfully!
                  </Alert>
                </Snackbar>
              </Box>
            </Grid>
          </Grid>
        </form>
      ) : (
        <>
          <Box className={classes.buttonContinue}>
            <h4>Your cart is empty</h4>
            <Link href="/shop" passHref>
              <Button variant="outlined">CONTINUE SHOPPING</Button>
            </Link>
          </Box>
        </>
      )}
    </>
  );
}
