import { makeStyles } from "@material-ui/core/styles";
import { Box, Container, Grid } from "@mui/material";
import { debounce } from "lodash";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import arrow_down from "../public/arrow_down.png";
import CartIcon from "./cart/CartIcon";
const useStyle_searchHeader = makeStyles({
  HeaderMidItem: {
    borderBottom: "1px solid #eee",
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: "#fff",
  },
  logo: {
    "@media (max-width: 899px)": {
      margin: "15px 0px",
      textAlign: "center",
    },
  },
  SearchHeader: {
    width: "100%",
    display: "flex",
    position: "relative",
    "& select": {
      width: " 25%",
      float: "left",
      height: "36px",
      paddingLeft: "15px",
      border: "1px solid #d1d3d4",
      borderRight: "none",
      borderRadius: "50px 0 0 50px",
      color: "#666",
      minWidth: "187px",
      appearance: "none",
      backgroundImage: `url("/arrow_down.png")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "right 10px top 14px",
      fontSize: "15px",
      fontFamily: "Mulish,'sans-serif'",
      fontWeight: 400,
      "&:focus": {
        outline: "none",
      },
      "@media (max-width: 599px)": {
        display: "none",
      },
    },
    "& input": {
      height: "36px",
      width: " 100%",
      padding: "0px 95px 0px 15px",
      border: "1px solid #d1d3d4",
      borderRadius: "0px 50px 50px 0px",
      color: "#666",
      fontSize: "15px",
      fontFamily: "Mulish,'sans-serif'",
      fontWeight: 400,
      "&:focus": {
        outline: "none",
      },
      "@media (max-width: 599px)": {
        borderRadius: "50px",
      },
    },
    "& button": {
      position: "absolute",
      right: "10px",
      top: "0px",
      border: "none",
      backgroundColor: "transparent",
      overflow: "hidden",
      width: "60px",
      height: "36px",
      padding: 0,
      borderLeft: "1px solid #d1d3d4",
      zIndex: 10,
      "&:hover": {
        cursor: "pointer",
      },
      "& svg": {
        fontSize: "25",
        margin: "7px -9px 0px 0px",
        color: "#444",
      },
    },
  },
  boxCart: {
    textAlign: "right",
  },
  cartHeader: {
    position: "relative",
    display: "inline-block",
    width: "30px",
    height: "30px",
    "& svg": {
      fontSize: "30px",
    },
  },
});
const HeaderMid = ({ categorized }) => {
  const classes = useStyle_searchHeader();
  const router = useRouter();
  const handleSearch = debounce((event) => {
    let term = event.target.value;
    const searchParams = new URLSearchParams();
    searchParams.set("resultSearch", term);
    const newUrl = `search?${searchParams.toString()}`;
    router.push(newUrl);
  }, 500);

  const changeSearchQuery = (value) => {
    const searchParams = new URLSearchParams();
    searchParams.set("category", value.target.value);
    const newUrl = `/shop?${searchParams.toString()}`;
    router.push(newUrl);
  };

  return (
    <>
      <div className={classes.HeaderMidItem}>
        <>
          <Container maxWidth="lg">
            <Box sx={{ flexGrow: 1 }}>
              <Grid container>
                <Grid item xs={12} sm={12} md={3}>
                  <div className={classes.logo}>
                    <Link href="/">
                      <a>
                        <Image
                          width={70}
                          height={40}
                          alt="image"
                          src="/ebaylogo.png"
                        />
                      </a>
                    </Link>
                  </div>
                </Grid>
                <Grid item xs={9} sm={10} md={8}>
                  <form className={classes.SearchHeader} id="searchform">
                    <select onChange={(e) => changeSearchQuery(e)}>
                      {categorized &&
                        categorized.map((items, index) => {
                          return (
                            <option key={`index${index}`} value={items.id}>
                              {" "}
                              {items.name}
                            </option>
                          );
                        })}
                    </select>
                    <input
                      type="text"
                      placeholder="Search name product...."
                      onChange={(event) => handleSearch(event)}
                    />
                  </form>
                </Grid>
                <Grid item xs={3} sm={2} md={1}>
                  <div className={classes.boxCart}>
                    <a className={classes.cartHeader}>
                      <CartIcon />
                    </a>
                  </div>
                </Grid>
              </Grid>
            </Box>
          </Container>
        </>
      </div>
    </>
  );
};

export default HeaderMid;
