import Typography from "@mui/material/Typography";
import { makeStyles } from "@material-ui/core/styles";
import React from 'react';

const colorHover = "#40c6ff";
const useStyles_SidebarTitle = makeStyles((theme) => ({
  titleSideBarCategory: {
    position: "relative",
    paddingBottom: "5px",
    marginBottom: "10px",
    marginTop: "17px",
    fontFamily: "Merriweather,sans-serif",
    fontWeight: "bold",
    fontSize: "18px",
    "&:before": {
      position: "absolute",
      content: '""',
      width: "60px",
      height: "1px",
      bottom: "-1px",
      backgroundColor: `${colorHover}`,
    },
  },
}))
function SidebarTitle({ title }) {
  const classes = useStyles_SidebarTitle();
  return (
    <Typography className={classes.titleSideBarCategory} component="h4" variant="h4">
      {title}
    </Typography>
  );
}

export default SidebarTitle;
