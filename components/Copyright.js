import { makeStyles } from "@material-ui/core/styles";
import { Box, Container, Grid } from "@mui/material";
import Image from "next/image";
import React from "react";
const colorHover = "#40c6ff";
const colorDefaul = "#666";
const colorBorder = "#f4f4f4";

const Item = () => {};
const useStyle_copyright = makeStyles({
  Copyright: {
    borderTop: " 1px solid",
    borderColor: `${colorBorder}`,
    color: `${colorDefaul}`,
    backgroundColor: "#fff",
    padding: "10px 0px",
    marginTop: " 7px",
    "& p": {
      padding: "0px",
      margin: "0px",
      "& a": {
        color: `${colorHover}`,
      },
    },
  },
  payment: {
    display: "flex",
    justifyContent: "right",
    alignItems: "center",
    "@media (max-width: 600px)": {
      justifyContent: "left",
      marginTop: "30px",
    },
  },
});
const Copyright = () => {
  const classes = useStyle_copyright();
  return (
    <div className={classes.Copyright}>
      <React.Fragment>
        <Container maxWidth="lg">
          <Box sx={{ flexGrow: 1 }}>
            <Grid container>
              <Grid item xs={12} sm={6} md={6}>
                <p>
                  Estore - Designed by{" "}
                  <a
                    href="http://netbasejsc.com/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Netbase JSC
                  </a>{" "}
                  . Copyright © 2006 - 2017. All Rights Reserved.
                </p>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
                <div className={classes.payment}>
                  <Image
                    width={200}
                    height={20}
                    alt="image"
                    src="/payment.png"
                  />
                </div>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </React.Fragment>
    </div>
  );
};
export default Copyright;
