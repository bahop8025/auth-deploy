import { makeStyles } from "@material-ui/core/styles";
import LocalMallOutlinedIcon from "@mui/icons-material/LocalMallOutlined";
import Link from "next/link";
import React, { useContext, useEffect, useState } from "react";
import { AppContextRest } from "../context/ContextRest";

const useStyles = makeStyles({
  containercart: {
    position: "relative",
    cursor: "pointer",
    "& p": {
      position: "absolute",
      bottom: "0px",
      right: "-13px",
      width: "auto",
      height: "auto",
      backgroundColor: "#40c6ff",
      display: "inline-flex",
      justifyContent: "center",
      alignItems: "center",
      color: "black",
      borderRadius: "50%",
      color: "#fff",
      padding: "3px 9px",
    },
  },
});

const CartIcon = () => {
  const classes = useStyles();
  const { totalQuantity } = useContext(AppContextRest);
  return (
    <>
      <Link href="/cart" passHref>
        <div className={classes.containercart}>
          <LocalMallOutlinedIcon />
          <p>{totalQuantity}</p>
        </div>
      </Link>
    </>
  );
};

export default CartIcon;
