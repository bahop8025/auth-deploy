import { makeStyles } from "@material-ui/core/styles";
import Link from "next/link";
import React, { useContext, useState } from "react";
import { AppContextRest } from "../context/ContextRest";
import { useEffect } from "react";

const colorHover = "#40c6ff";
const useStyles = makeStyles({
  btnviewcart: {
    margin: "25px",
  },
  AddToCartButton: {
    borderRadius: "20px",
    backgroundColor: `${colorHover}`,
    height: "40px",
    lineHeight: "34px",
    padding: "0px 30px",
    color: "#fff",
    border: "none",
    textTransform: "uppercase",
    fontSize: "16px",
    marginTop: "20px",
    "&:hover": {
      cursor: "pointer",
    },
    "&:last-child": {
      // marginLeft: "20px"
    },
  },
});

const AddToCartButton = ({ product, quantity }) => {
  const classes = useStyles();
  const { muchAddToCart } = useContext(AppContextRest);
  const [isAddedToCart, setIsAddedToCart] = useState(false);

  const addToCart = (e) => {
    e.preventDefault();
    setIsAddedToCart(true);
    muchAddToCart(product, quantity);
  };

  return (
    <>
      {!isAddedToCart ? (
        <form onSubmit={addToCart}>
          <button className={classes.AddToCartButton} type="submit">
            {" "}
            Adding To Cart
          </button>
        </form>
      ) : (
        <div>
          <p style={{ color: "#00FF00" }}>
            Product added to cart successfully!
          </p>
          <Link href="/cart" passHref>
            <button className={classes.AddToCartButton}>View Cart</button>
          </Link>
        </div>
      )}
    </>
  );
};

export default AddToCartButton;
