import {
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
} from "@mui/material";
import ListItem from "@mui/material/ListItem";
import React, { useState } from "react";

function Shipping({ isResult }) {
  const [selectedOption, setSelectedOption] = useState(
    isResult[1].settings.cost.value
  );
  window.localStorage.setItem("selectedOption", selectedOption);
  function handleOptionChange(event) {
    const value = event.target.value;
    setSelectedOption(value);
    if (typeof window !== "undefined") {
      window.localStorage.setItem("selectedOption", value);
    }
  }

  return (
    <>
      <ListItem>
        <FormControl>
          <RadioGroup
            name="options"
            value={selectedOption}
            onChange={handleOptionChange}
          >
            {isResult &&
              isResult.map((option) => {
                return (
                  <FormControlLabel
                    style={{ marginBottom: "5px" }}
                    key={option.id}
                    value={option?.settings?.cost?.value}
                    control={<Radio />}
                    label={option.method_title}
                  />
                );
              })}
          </RadioGroup>
        </FormControl>
      </ListItem>
    </>
  );
}

export default Shipping;
