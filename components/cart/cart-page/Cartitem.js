import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import { useContext, useState } from "react";
import { AppContextRest } from "../../context/ContextRest";
import { Button, Link } from "@material-ui/core";
import Image from "next/image";

const CartItem = ({ product }) => {
  const { updateQuantity, removeProductFromCart } = useContext(AppContextRest);
  const [quantity, setQuantity] = useState(product?.quantity);

  const handleIncreaseQuantity = () => {
    const newQuantity = quantity + 1;
    setQuantity(newQuantity);
    updateQuantity(product.id, newQuantity);
  };

  const handleDecreaseQuantity = () => {
    if (quantity > 0) {
      const newQuantity = quantity - 1;
      setQuantity(newQuantity);
      updateQuantity(product.id, newQuantity);
    }
  };

  return (
    <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
      <TableCell align="center">
        <b
          onClick={() => {
            removeProductFromCart(product.id);
          }}
        >
          <DeleteForeverIcon />
        </b>
      </TableCell>
      <TableCell align="center">
        <Link href={`/product/${product?.slug}`}>
          <a>
            <Image
              height={150}
              width={150}
              src={product?.images?.[0].src}
              alt="Card image cap"
            />
          </a>
        </Link>
      </TableCell>
      <TableCell align="center">
        <Typography>
          <Link
            href={`/product/${product?.slug}`}
            style={{ textDecoration: "none" }}
          >
            <a>{product?.name}</a>
          </Link>
        </Typography>
      </TableCell>
      <TableCell align="center">
        <Button variant="outlined" onClick={handleDecreaseQuantity}>
          -
        </Button>
        <span style={{ margin: "0 5px 0 5px" }}>{quantity}</span>
        <Button variant="outlined" onClick={handleIncreaseQuantity}>
          +
        </Button>
      </TableCell>
      <TableCell align="center">
        <Typography>x</Typography>
      </TableCell>
      <TableCell align="center">
        <Typography>${product?.price * product.quantity}</Typography>
      </TableCell>
    </TableRow>
  );
};

export default CartItem;
