import { makeStyles } from "@material-ui/core/styles";
import DeleteIcon from "@mui/icons-material/Delete";
import SendIcon from "@mui/icons-material/Send";
import { Button, Grid } from "@mui/material";
import Box from "@mui/material/Box";
import Chip from "@mui/material/Chip";
import Divider from "@mui/material/Divider";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import Link from "next/link";
import { useRouter } from "next/router";
import * as React from "react";
import { useContext, useEffect, useState } from "react";
import { AppContextRest } from "../../context/ContextRest";
import CartItem from "./Cartitem";
import Shipping from "./Shipping";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};
const useStyles_cart = makeStyles((theme) => ({
  wooNextCartWrapper: {
    display: "flex",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },
  buttonCheckout: {
    textAlign: "center",
    marginTop: "10px",
    marginBottom: "15px",
    paddingBottom: "20px",
  },
  cartTotal: {
    background: "#fff",
    marginTop: "30px",
  },
  tableCart: {
    paddingRight: "50px",
  },
  buttonContinue: {
    marginTop: "20px",
  },
  cartTotalLeft: {
    paddingTop: "16px",
  },
  tableItem: {
    display: "flex",
    margin: "auto",
    gap: 60,
  },
}));

const CartItemsContainer = ({ shipping }) => {
  const { cart, clearCart, totalQuantity } = useContext(AppContextRest);
  const router = useRouter();
  const classes = useStyles_cart();
  const handleOpen = () => router.push("/checkout");
  const [total, setTotal] = useState();
  const [isShowButtonCheckOut, setIsShowButtonCheckOut] = useState(false);
  const [isResult, setIsResult] = useState(null);

  useEffect(() => {
    setTotal(
      cart.reduce((acc, curr) => acc + Number(curr.price) * curr.quantity, 0)
    );
  }, [cart]);

  useEffect(() => {
    setIsResult(shipping);
    setIsShowButtonCheckOut(true);
  }, [shipping]);

  return (
    <>
      <div className={classes.cartMain}>
        {cart.length > 0 ? (
          <div className={classes.wooNextCartWrapper}>
            <Grid item lg={9}>
              <Box className={classes.tableCart}>
                <Chip
                  label="Clear All Cart"
                  onClick={() => clearCart()}
                  deleteIcon={<DeleteIcon />}
                  variant="outlined"
                />
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">
                        <Typography variant="h6">#</Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography variant="h6">Image</Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography variant="h6">Name</Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography variant="h6">Quantity</Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography variant="h6"></Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography variant="h6">Price</Typography>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {cart.map((product) => (
                      <CartItem product={product} key={product.id} />
                    ))}
                  </TableBody>
                </Table>

                <Divider />
                <Box className={classes.buttonContinue}>
                  <Link href="/shop">
                    <a>
                      <Button variant="outlined">CONTINUE SHOPPING</Button>
                    </a>
                  </Link>
                </Box>
              </Box>
            </Grid>

            <Grid item lg={3}>
              <Box className={classes.cartTotalLeft}>
                <Paper className={classes.cartTotal}>
                  <List>
                    <Typography
                      style={{ marginLeft: "16px" }}
                      component="h5"
                      variant="h5"
                    >
                      Cart Total
                    </Typography>
                    <ListItem>
                      <ListItemText>Total:</ListItemText>
                      <Typography variant="h5">${total}</Typography>
                    </ListItem>
                    <ListItem>
                      <ListItemText>Quantity:</ListItemText>
                      <Typography variant="h6">{totalQuantity}</Typography>
                    </ListItem>
                    <Divider />
                    <Shipping isResult={shipping} />
                    <Divider />
                  </List>
                  <Box className={classes.buttonCheckout}>
                    {isShowButtonCheckOut && (
                      <Button
                        variant="outlined"
                        endIcon={<SendIcon />}
                        onClick={handleOpen}
                      >
                        Proceeed to checkout
                      </Button>
                    )}
                  </Box>
                </Paper>
              </Box>
            </Grid>
          </div>
        ) : (
          <>
            <Box className={classes.buttonContinue}>
              <p>Your cart is empty</p>
              <Link href="/shop">
                <a>
                  <Button variant="outlined">CONTINUE SHOPPING</Button>
                </a>
              </Link>
            </Box>
          </>
        )}
      </div>
    </>
  );
};

export default CartItemsContainer;
