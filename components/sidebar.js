import { makeStyles } from "@material-ui/core/styles";
import LanguageIcon from "@mui/icons-material/Language";
import LoginIcon from "@mui/icons-material/Login";
import LogoutIcon from "@mui/icons-material/Logout";
import { Box, Container, Grid } from "@mui/material";
import { useRouter } from "next/router";
import React, { useContext } from "react";
import arrow_down from "../public/arrow_down.png";
import { AppContextRest } from "./context/ContextRest";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { useState } from "react";

const colorHeading = "#323232";
const colorHover = "#40c6ff";
const colorDefaul = "#666";

const useStyle_HeaderTop = makeStyles({
  headerTop: {
    padding: "10px",
    backgroundColor: "#f3f3f3",
  },
  menuTop: {
    listStyle: "none",
    padding: "0px",
    margin: "0px",
    "& li a": {
      color: "#999",
      float: "left",
      padding: "0px 10px",
      position: "relative",
      "&:first-child": {
        paddingLeft: "0px",
        "&::before": {
          display: "none",
        },
      },
      "&::before": {
        position: "absolute",
        content: '""',
        width: "1px",
        height: "14px",
        left: "0px",
        top: "6px",
        backgroundColor: "#999999",
      },
      "& a": {
        color: "#999",
        fontSize: "14px",
        fontFamily: "Mulish,'sans-serif'",
        "&:hover": {
          color: `${colorHover}`,
        },
      },
    },
  },
  headerTopRight: {
    display: "flex",
    listStyle: "none",
    padding: "0px",
    margin: "0px",
    float: "right",
    "& li": {
      float: "left",
      padding: "0px 10px",
      position: "relative",
      display: "inline-flex",
      alignItems: "center",
      "&:first-child": {
        paddingLeft: "0px",
        "&::before": {
          display: "none",
        },
      },
      "&::before": {
        position: "absolute",
        content: '""',
        width: "1px",
        height: "14px",
        left: "0px",
        top: "4px",
        backgroundColor: "#999999",
      },
      "& a": {
        color: "#999",
        fontSize: "14px",
        fontFamily: "Mulish,'sans-serif'",
        display: "inline-flex",
        alignItems: "center",
        "&:hover": {
          color: `${colorHover}`,
        },
        "& svg": {
          fontSize: "14px",
          marginRight: "5px",
        },
      },
      "& svg": {
        fontSize: "14px",
        marginRight: "5px",
        color: "#999",
      },
      "& select": {
        border: "none",
        color: "#999",
        fontSize: "14px",
        fontFamily: "Mulish,'sans-serif'",
        "&:focus": {
          outline: "none",
        },
      },
    },
    "@media (max-width: 899px)": {
      textAlign: "center",
      float: "none",
      "& li": {
        float: "none",
        display: "inline-block",
      },
    },
    "@media (max-width: 425px)": {
      "& li": {
        "& span": {
          display: "none",
        },
      },
    },
  },
  curency: {
    "& select": {
      paddingRight: "15px",
      background: "transparent",
      appearance: "none",
      backgroundImage: `url("/arrow_down.png")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "right 1px top 6px;",
    },
  },
  language: {
    "& select": {
      background: "transparent",
      appearance: "none",
      backgroundImage: `url("/arrow_down.png")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "right 1px top 7px;",
    },
  },
  showLogin: {
    cursor: "pointer",
    fontSize: "15px",
    display: "flex",
    "@media (max-width: 768px)": {
      display: "flex",
      justifyContent: "center",
    },
  },
});

const SideBarTop = () => {
  const router = useRouter();
  const classes = useStyle_HeaderTop();
  const [open, setOpen] = useState(false);
  const { user, logoutUser } = useContext(AppContextRest);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleLogout = () => {
    logoutUser();
    setOpen(false);
    if (router.pathname === "/checkout") {
      router.push("/myaccount"); // Chuyển hướng đến trang đăng nhập chỉ khi đang ở trang Checkout
    } else if (router.pathname === "") {
      router.push("");
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleLogin = () => {
    router.push("/myaccount");
  };

  return (
    <div className={classes.headerTop}>
      <Container>
        <Grid container spacing={2}>
          <Grid
            item
            sx={{ display: { xs: "none", sm: "none", md: "block" } }}
            md={6}
          >
            <Box>
              <ul className={classes.menuTop}>
                <li>
                  <a href="tel:1800123456">Call Support Free:1800123456</a>
                </li>
              </ul>
            </Box>
          </Grid>
          <Grid item xs={12} sm={12} md={6}>
            <Box>
              <ul className={classes.headerTopRight}>
                <li className={classes.curency}>
                  <select>
                    <option value="">USD, $</option>
                    <option value="">EUR, €</option>
                  </select>
                </li>
                <li className={classes.language}>
                  <LanguageIcon />
                  <select>
                    <option value="">English</option>
                    <option value="">Viet Nam</option>
                  </select>
                </li>
                {user ? (
                  <span className={classes.showLogin}>
                    <span onClick={handleClickOpen} style={{ display: "flex" }}>
                      <span style={{ paddingRight: "10px" }}>
                        Welcome,{" "}
                        <span style={{ color: "#40c6ff" }}>{user.name}!</span>
                      </span>
                      Logout{" "}
                      <span>
                        <LogoutIcon />
                      </span>
                    </span>
                  </span>
                ) : (
                  <span
                    style={{ cursor: "pointer", display: "flex" }}
                    onClick={handleLogin}
                  >
                    Login{" "}
                    <span>
                      <LoginIcon />
                    </span>
                  </span>
                )}
              </ul>
            </Box>
          </Grid>
        </Grid>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Do you want to sign out !
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleLogout}>Disagree</Button>
            <Button onClick={handleClose} autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>
      </Container>
    </div>
  );
};
export default SideBarTop;
