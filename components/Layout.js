import React from "react";
import AddHead from "./AddHead";
import Copyright from "./Copyright";
import Footer from "./Footer";
import HeaderCenter from "./HeaderCenter";
import HeaderMid from "./HeaderMid";
import { AppProviderRest } from "./context/ContextRest";
import SideBarTop from "./sidebar";
import { ToastContainer } from "react-toastify";

const Layout = (props) => {
  const { categorized } = props

  return (
    <AppProviderRest>
      <div>
        <AddHead />
        <SideBarTop />
        <HeaderMid categorized={categorized} />
        <HeaderCenter categorized={categorized} />
        {props.children}
        <Footer />
        <Copyright />
      </div>
      <ToastContainer
        position="bottom-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
    </AppProviderRest>
  );
};
export default Layout;
