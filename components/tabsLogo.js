import { makeStyles } from "@material-ui/core/styles";
import { Container, Grid, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Image from "next/image";
import * as React from "react";

const colorHover = "#40c6ff";
const useStyles = makeStyles({
  listLogo: {
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 768px)": {
      display: "block",
      with: "100%",
    },
  },
  logoGroup: {
    display: "flex",
    marginLeft: "-9px",
    "@media (max-width: 350px)": {
      display: "block",
      with: "100%",
    },
  },
  imgLogo: {
    width: "100%",
    display: "block",
    border: "1px solid #efefef",
    marginLeft: "15px",
    marginRight: "15px",
    marginBottom: "30px",
  },
  titleHome: {
    textAlign: "center",
    fontFamily: "Merriweather,sans-serif",
    color: "#444444",
    fontSize: "24px",
    fontWeight: "bold",
    margin: "30px 0px 25px 0px",
    "& span": {
      position: "relative",
      display: "inline-block",
      "&::after": {
        position: "absolute",
        content: '""',
        width: "15px",
        height: "1px",
        left: "-30px",
        top: "18px",
        backgroundColor: `${colorHover}`,
      },
      "&::before": {
        position: "absolute",
        content: '""',
        width: "15px",
        height: "1px",
        right: "-30px",
        top: "18px",
        backgroundColor: `${colorHover}`,
      },
    },
  },
});

export default function Logo() {
  const classes = useStyles();
  return (
    <>
      <Container>
        <Box>
          <Typography className={classes.titleHome}>
            <span>OUTLET BRAND STUFF</span>
          </Typography>
        </Box>
        <Grid>
          <Box className={classes.listLogo}>
            <Box className={classes.logoGroup}>
              <Box className={classes.imgLogo}>
                <Image width={150} height={150} src="/logo1.png" alt="Image" />
              </Box>
              <Box className={classes.imgLogo}>
                <Image width={150} height={150} src="/logo2.png" alt="Image" />
              </Box>
            </Box>
            <Box className={classes.logoGroup}>
              <Box className={classes.imgLogo}>
                <Image width={150} height={150} src="/logo3.png" alt="Image" />
              </Box>
              <Box className={classes.imgLogo}>
                <Image width={150} height={150} src="/logo4.png" alt="Image" />
              </Box>
            </Box>
            <Box className={classes.logoGroup}>
              <Box className={classes.imgLogo}>
                <Image width={150} height={150} src="/logo5.png" alt="Image" />
              </Box>
              <Box className={classes.imgLogo}>
                <Image width={150} height={150} src="/logo6.png" alt="Image" />
              </Box>
            </Box>
          </Box>
        </Grid>
      </Container>
    </>
  );
}
