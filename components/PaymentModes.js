import { FormControl, FormControlLabel, Radio, RadioGroup, } from "@mui/material";
import { Box } from "@mui/system";
const PaymentModes = ({ paymentMethod, handleOnChange, handleLabelChange }) => {
  return (
    <Box className="mt-3">
      <FormControl>
        <RadioGroup
          aria-labelledby="demo-radio-buttons-group-label"
          name="radio-buttons-group"
          defaultValue="cod"
        >
          <FormControlLabel
            onChange={handleOnChange}
            value="bacs"
            control={<Radio />}
            name="paymentMethod"
            label="Direct Bank Transfer"
            checked={paymentMethod === "bacs"}
            onClick={() => handleLabelChange("Direct Bank Transfer")}
          />
          <FormControlLabel
            onChange={handleOnChange}
            value="paypal"
            name="paymentMethod"
            control={<Radio />}
            label="Pay with Paypal"
            checked={paymentMethod === "paypal"}
            onClick={() => handleLabelChange("Pay with Paypal")}
          />
          <FormControlLabel
            onChange={handleOnChange}
            value="cod"
            control={<Radio />}
            name="paymentMethod"
            label="Cash on Delivery"
            checked={paymentMethod === "cod"}
            onClick={() => handleLabelChange("Cash on Delivery")}
          />
        </RadioGroup>
      </FormControl>
    </Box>
  );
};

export default PaymentModes;
