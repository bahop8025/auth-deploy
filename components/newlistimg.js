import { makeStyles } from "@material-ui/core/styles";
import { Link } from "@mui/material";
import Box from "@mui/material/Box";
import * as React from "react";

const useStyles = makeStyles({
  wrapperImage: {
    display: "flex",
    marginTop: "50px",

    "@media (max-width: 768px)": {
      display: "block",
    },
  },
  rowImage: {
    display: "flex",
    marginBottom: "20px",
    "@media (max-width: 600px)": {
      display: "block",
      marginBottom: "30px",
    },
  },
  box_1Image: {
    boxSizing: "border-box",
    paddingRight: "15px",
    "@media (max-width: 768px)": {
      paddingRight: "0px",
      marginBottom: "30px",
    },
  },
  box_Image: {
    marginBottom: "30px",
  },
  box_2Image: {
    paddingRight: "15px",
    paddingLeft: "15px",
    "@media (max-width: 768px)": {
      paddingRight: "0px",
      marginBottom: "30px",
    },
    "@media (max-width: 600px)": {
      paddingLeft: "0px",
    },
  },
  box_3Image: {
    paddingRight: "15px",
    paddingLeft: "15px",
    "@media (max-width: 768px)": {
      width: "100%",
      paddingLeft: "0px",
      marginBottom: "30px",
    },
    "@media (max-width: 600px)": {
      paddingRight: "0px",
    },
  },
  box_4Image: {
    paddingLeft: "15px",
    "@media (max-width: 768px)": {
      width: "100%",
    },
    "@media (max-width: 600px)": {
      paddingLeft: "0px",
    },
  },
  Image: {
    width: "100% !important",
    height: "auto !important",
    transition: "all .5s",
    "@media (max-width: 768px)": {
      width: "100%",
    },
  },
});

export default function NewImageList() {
  const classes = useStyles();
  return (
    <>
      <Box className={classes.wrapperImage}>
        <Box className={classes.rowImage}>
          <Box className={classes.box_1Image}>
            <Box className={classes.box_Image}>
              <Link href="/product/apple-iphone-7-plus">
                <a>
                  <img
                    alt="Image"
                    src="/image_1.png"
                    className={classes.Image}
                  />
                </a>
              </Link>
            </Box>
            <Box>
              <Link href="/product/ring-with-12-round-diamond/">
                <a>
                  <img
                    alt="Image"
                    src="/image_2.png"
                    className={classes.Image}
                  />
                </a>
              </Link>
            </Box>
          </Box>
          <Box className={classes.box_2Image}>
            <Box>
              <Link href=" /shop">
                <img alt="Image" src="/image_3.png" className={classes.Image} />
              </Link>
            </Box>
          </Box>
        </Box>

        <Box className={classes.rowImage}>
          <Box className={classes.box_3Image}>
            <Box className={classes.box_Image}>
              <Link href="/product/lenovo-15-e460-notebook">
                <a>
                  <img
                    alt="Image"
                    src="/image_4.png"
                    className={classes.Image}
                  />
                </a>
              </Link>
            </Box>
            <Box>
              <Link href="/product/wireless-mouse-m187">
                <a>
                  <img
                    alt="Image"
                    src="/image_5.png"
                    className={classes.Image}
                  />
                </a>
              </Link>
            </Box>
          </Box>
          <Box className={classes.box_4Image}>
            <Box className={classes.box_Image}>
              <Link href="/product/dz09-smart-watch">
                <a>
                  <img
                    alt="Image"
                    src="/image_6.png"
                    className={classes.Image}
                  />
                </a>
              </Link>
            </Box>
            <Box>
              <Link href="/product/lum-5d-mark-iv-dslr-camera">
                <a>
                  <img
                    alt="Image"
                    src="/image_7.png"
                    className={classes.Image}
                  />
                </a>
              </Link>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
}
