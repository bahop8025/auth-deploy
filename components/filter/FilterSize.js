import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import React, { useState } from 'react';


function FilterSize({ sizeProduct, onRadioChange }) {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [selectedOption, setSelectedOption] = useState('');

  const handleSearchChange = (event) => {
    const searchTerm = event.target.value;
    setSearchTerm(searchTerm);

    const filteredResults = sizeProduct.filter((option) =>
      option.name.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setSearchResults(filteredResults);
  };

  const handleChange = (event) => {
    setSelectedOption(event.target.value);
    onRadioChange(event.target.value)
  };


  return (
    <>
      <FormControl fullWidth>
        <Select
          value={selectedOption}
          onChange={handleChange}
          className="select-input"
          variant="outlined"
        >
          <input
            style={{
              width: " -webkit-fill-available",
              margin: "5px 15px ",
              padding: "7px",
              borderRadius: "5px",
              outline: "none",
              border: "1px solid #757575"
            }}
            value={searchTerm}
            onChange={handleSearchChange}
            label="Search"
            variant="outlined"
            className="search-input"
            placeholder='Search L,M,S...'
          />
          {searchResults.map((option) => (
            <MenuItem key={option.id} value={option}>
              {option.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </>
  )
}

export default FilterSize
