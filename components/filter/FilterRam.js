import CheckBoxIcon from "@mui/icons-material/CheckBox";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import Autocomplete from "@mui/material/Autocomplete";
import Checkbox from "@mui/material/Checkbox";
import TextField from "@mui/material/TextField";
import React from "react";

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

import { useState } from "react";

function FilterRam({ ramProduct, onRadioChange }) {
  const [selectedValues, setSelectedValues] = useState([]);

  const handleChange = (event, value) => {
    const selectedItems = value.map((item) => ({
      id: item.id,
      slug: item.slug_parent,
    }));
    setSelectedValues(selectedItems);
    onRadioChange(selectedItems);
  };
  return (
    <Autocomplete
      multiple
      id="checkboxes-tags-demo"
      options={ramProduct}
      disableCloseOnSelect
      getOptionLabel={(option) => option.name}
      renderOption={(props, option, { selected }) => (
        <li {...props}>
          <Checkbox
            icon={icon}
            checkedIcon={checkedIcon}
            style={{ marginRight: 8 }}
            checked={selected}
          />
          {option.name}
        </li>
      )}
      fullWidth
      style={{ padding: "0px !important" }}
      renderInput={(params) => (
        <TextField {...params} label="Product ram" placeholder="Product ram" />
      )}
      onChange={handleChange}
    />
  );
}

export default FilterRam;
