import React, { useState } from 'react';
import { makeStyles } from "@material-ui/core/styles";
const useStyles_FilterSelect = makeStyles((theme) => ({
  filterSelect: {
    width: "100%",
    padding: "15px 7px 15px 7px",
    outline: "none",
    borderRadius: "5px",
    border: "1px solid #757575",
    fontSize: "1rem",
    "& option": {
      fontFamily: "Roboto, Helvetica, Arial, sans-serif",
      fontSize: "1rem"
    }
  }
}))

function FilterSelect({ onSortChange }) {
  const classes = useStyles_FilterSelect();
  const [valueSelect, setValueSelect] = useState('');

  const handleChange = (event) => {
    const selectedValue = event.target.value;
    setValueSelect(selectedValue)
    onSortChange(selectedValue);
  };

  return (
    <>
      <select
        className={classes.filterSelect}
        value={valueSelect}
        onChange={handleChange}
      >
        <option value="menu_order">Default sorting</option>
        <option value="popularity">Sort by popularity</option>
        <option value="rating">Sort by average rating</option>
        <option value="date">Sort by latest</option>
        {/* <option value="price">Sort by price: low to high</option>
        <option value="price-desc">Sort by price: high to low</option> */}
      </select>
    </>
  )
}

export default FilterSelect
