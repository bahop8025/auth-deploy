import {
  Container,
  Link,
  TableCell,
  TableHead,
  Typography,
} from "@material-ui/core";
import { Box } from "@mui/material";
import { makeStyles } from "@material-ui/core/styles";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useContext } from "react";
import { AppContextRest } from "../components/context/ContextRest";
import { getCategoriesData } from "../utils/categories";
import { getProductsData } from "../utils/product";

const colorHeading = "#323232";
const colorHover = "#40c6ff";
const color = "rgb(64,198,255)";
const styleOrder = makeStyles({
  thankYou: {
    color: `${color}`,
    margin: "30px 0 20px 0",
  },
  titleOrder: {
    fontWeight: 700,
    color: `${colorHeading}`,
    fontSize: "35px",
  },
  stylesstatus: {
    color: `${colorHover}`,
    padding: "0px",
    margin: "0px",
  },
  head: {
    display: "flex",
    justifyContent: "space-between",
  },
  table: {
    width: "100%",
    border: "1px solid",
  },
  shipping: {
    fontWeight: 700,
    color: `${colorHeading}`,
    fontSize: "20px",
  },
  des: {
    display: "flex",
    justifyContent: "space-around",
  },
  subTotal: {
    textAlign: "end",
    fontSize: "20px",
    color: `${color}`,
  },
  status: {
    color: "#00FF00",
  },
});
const OrderSuccess = (props) => {
  const classes = styleOrder();
  const router = useRouter();
  const { query } = router;
  const orderId = query.orderId;
  const [orderSuccess, setOrderSuccess] = useState();
  const { resetQuantity, emptyCart } = useContext(AppContextRest);

  useEffect(() => {
    resetQuantity();
    emptyCart();
    try {
      const data = JSON.parse(decodeURIComponent(orderId));
      if (data === undefined || "" || null) {
        return null;
      } else {
        setOrderSuccess(data);
      }
    } catch (error) {
      // console.log(error);
    }
  }, [orderId, emptyCart, resetQuantity]);

  return (
    <Container>
      <Box className="container">
        {orderSuccess ? (
          <div>
            <div className={classes.head}>
              <p className={classes.thankYou}>
                Thank you. Your order has been received.
              </p>
              <Link
                href="/"
                style={{
                  display: "flex",
                  alignItems: "center",
                  cursor: "pointer",
                }}
              >
                <p> Back to home page</p>
              </Link>
            </div>
            <div className={classes.orderDetails}>
              <p className={classes.titleOrder}>Order details</p>
              <p>
                Id Order : <strong>{orderSuccess.id}</strong>
              </p>
              <p>
                Sub total : <strong>${orderSuccess?.total}</strong>
              </p>
              <p>
                Phone : <strong>{orderSuccess?.billing?.phone}</strong>
              </p>
              <p>
                Email : <strong>{orderSuccess?.billing?.email}</strong>
              </p>
              <p>
                Payment method : <strong>{orderSuccess.payment_method}</strong>
              </p>
              <p>
                date : <strong>{orderSuccess.date_created}</strong>
              </p>
              <p>
                Phone : <strong>{orderSuccess?.billing?.phone}</strong>
              </p>
              <p>
                Shipping :{" "}
                {orderSuccess.shipping_lines &&
                  orderSuccess.shipping_lines.map((item, index) => (
                    <strong key={`index${index}`}>${item.total}</strong>
                  ))}
              </p>
              <p>
                Tax : <strong>${orderSuccess?.shipping_tax}</strong>
              </p>
              <p className={classes.status}>Status : {orderSuccess.status}</p>
            </div>
            <div>
              <p className={classes.shipping}>Products</p>
              <table className={classes.table}>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  {orderSuccess.line_items &&
                    orderSuccess.line_items.map((item) => (
                      <tr
                        style={{ textAlign: "center", height: "40px" }}
                        key={item.id}
                      >
                        <td>{item.name}</td>
                        <td>{item.quantity}</td>
                        <td>{item.total}</td>
                      </tr>
                    ))}
                </tbody>
              </table>
              <p className={classes.subTotal}>
                Sub total : <strong>${orderSuccess?.total}</strong>
              </p>
            </div>
            <div className={classes.des}>
              <div>
                <p className={classes.shipping}>Billing address</p>
                <p>
                  First nmae :{" "}
                  <strong>{orderSuccess.billing.first_name}</strong>
                </p>
                <p>
                  Last Name : <strong>{orderSuccess.billing.last_name}</strong>
                </p>
                <p>
                  Address 1 : <strong>{orderSuccess.billing.address_1}</strong>
                </p>
                <p>
                  Address 2 :<strong>{orderSuccess.billing.address_2}</strong>
                </p>
                <p>
                  City : <strong>{orderSuccess.billing.city}</strong>
                </p>
                <p>
                  Company : <strong>{orderSuccess.billing.company}</strong>
                </p>
                <p>
                  Email : <strong>{orderSuccess.billing.email}</strong>
                </p>
                <p>
                  Country : <strong>{orderSuccess.billing.country}</strong>
                </p>
                <p>
                  State / County : <strong>{orderSuccess.billing.state}</strong>
                </p>
                <p>
                  Phone :<strong>{orderSuccess.billing.phone}</strong>
                </p>
              </div>
              <div>
                <p className={classes.shipping}>Shipping address</p>
                <p>
                  First Name :{" "}
                  <strong>{orderSuccess.shipping.first_name}</strong>
                </p>
                <p>
                  Last Name : <strong>{orderSuccess.shipping.last_name}</strong>
                </p>
                <p>
                  Address 1 : <strong>{orderSuccess.shipping.address_1}</strong>
                </p>
                <p>
                  Address 2 : <strong>{orderSuccess.shipping.address_2}</strong>
                </p>
                <p>
                  City : <strong>{orderSuccess.shipping.city}</strong>
                </p>
                <p>
                  Company : <strong>{orderSuccess.shipping.company}</strong>
                </p>
                <p>
                  Country : <strong>{orderSuccess.shipping.country}</strong>
                </p>
                <p>
                  State / County :{" "}
                  <strong>{orderSuccess.shipping.state}</strong>
                </p>
              </div>
            </div>
          </div>
        ) : (
          <>
            <Link href="/">
              <p>Back to home page</p>
            </Link>
          </>
        )}
      </Box>
    </Container>
  );
};

export default OrderSuccess;
export async function getServerSideProps(context) {
  const { category, page, result } = context.query;

  const searchValue = result || "";
  const { products, totalPages, totalProducts } = await getProductsData(
    4,
    page || 1,
    category,
    searchValue
  );
  const { categories } = await getCategoriesData();

  const categorized = categories.filter((cate) => {
    const isUncategorizedCategory = cate.slug === "uncategorized";
    return !isUncategorizedCategory;
  });

  return {
    props: {
      products,
      totalPages,
      totalProducts,
      searchValue: searchValue,
      categorized,
    },
  };
}
