import { yupResolver as yupResolverLogin, yupResolver as yupResolverRegister } from "@hookform/resolvers/yup";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@mui/material/Box";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { useRouter } from "next/router";
import * as React from "react";
import { useContext } from "react";
import { useForm as useFormLogin, useForm as useFormRegister } from "react-hook-form";
import { toast } from "react-toastify";
import * as yup from "yup";
import InputForm from "../components/checkout/InputForm";
import { AppContextRest } from "../components/context/ContextRest";
import { getCategoriesData } from "../utils/categories";
import { getLogin } from "../utils/login";

const useStyles = makeStyles({
  pageAccout: {
    marginBottom: "60px",
    minHeight: "200px",
    background: `url("/banner_page.png")`,
    backgroundSize: "cover",
  },

  titlePage: {
    display: "flex",
    paddingTop: "75px",
    paddingBottom: "75px",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },

  textTile: {
    color: "white",
    fontFamily: "Merriweather",
    fontSize: "50px",
    fontWeight: 700,
  },
  titleText: {
    color: "white",
    fontFamily: "Muli",
    fontWeight: 400,
    fontSize: "14px",
  },
  rightTextPage: {
    paddingTop: "12px",
    float: "right",
    "@media (max-width: 768px)": {
      float: "inherit",
    },
  },
  inputAccout: {
    width: "100%",
    minHeight: "55px",
    paddingLeft: "10px",
    marginTop: "10px",
  },
  formPage: {
    display: "flex",
    border: "1px solid #ccc",
    padding: "30px",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },
  loginPage: {
    "@media (max-width: 768px)": {
      marginBottom: "40px",
    },
  },
  formLogin: {
    width: "100%",
    paddingRight: "25px",
  },

  regisPage: {
    "@media (max-width: 768px)": {
      marginTop: "40px",
    },
  },
  textFormPage: {
    marginBottom: "30px",
    textAlign: "left",
    fontFamily: "Merriweather",
    fontWeight: 700,
    fontStyle: "normal",
  },
});

const schemaLogin = yup.object({
  email: yup
    .string()
    .required("Email is required")
    .email("Email is not valid."),
  password: yup
    .string()
    .required("Password is required")
    .min(6, "Password must be at least 6 characters"),
});
const schemaRegister = yup.object({
  email: yup
    .string()
    .required("Email is required")
    .email("Email is not valid."),
  password: yup
    .string()
    .required("Password is required")
    .min(6, "Password must be at least 6 characters"),
  // confirmPassword: yup
  //   .string()
  //   .oneOf([yup.ref("password"), null], "Passwords must match"),
});

export default function MyAccout({ usersLogin }) {
  const { loginUser } = useContext(AppContextRest);
  const router = useRouter();
  const classes = useStyles();

  const {
    handleSubmit: handleSubmitLogin,
    register: registerLogin,
    formState: { errors: errorsLogin },
    reset: resetLogin,
  } = useFormLogin({
    resolver: yupResolverLogin(schemaLogin),
  });

  const {
    handleSubmit: handleSubmitRegister,
    register: registerRegister,
    formState: { errors: errorsRegister },
    reset: resetRegister,
  } = useFormRegister({
    resolver: yupResolverRegister(schemaRegister),
  });

  const fromSubmitLogin = async (data) => {
    const { email, password } = data;
    const user = usersLogin.find(
      (user) => user.email === email && user.phone === password
    );
    if (user) {
      loginUser(user);
      router.push("/checkout");
      toast.success("Login successful");
      resetLogin();
    } else {
      toast.error("Login failed");
    }
  };

  const fromSubmitRegister = async (data) => {
    try {
      // const { email, password } = data;
      // const isEmailAlreadyRegistered = (email) => {
      //   return usersLogin.some((user) => user.email === email);
      // };

      // if (isEmailAlreadyRegistered(email)) {
      //   toast.error("Email is already registered");
      //   return;
      // }

      const response = await fetch("/api/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });

      if (response.ok) {
        resetRegister();
        router.push("/myaccount");
        toast.success("Register successful");
      } else {
        throw new Error("Register failed");
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <Box className={classes.pageAccout}>
        <Container>
          <Box className={classes.titlePage}>
            <Grid container spacing={1}>
              <Grid item lg={6} md={6} xs={12}>
                <Box>
                  <Typography
                    className={classes.textTile}
                    component="h3"
                    variant="h3"
                  >
                    My account
                  </Typography>
                </Box>
              </Grid>
              <Grid item lg={6} md={6} xs={12}>
                <Box className={classes.rightTextPage}>
                  <Breadcrumbs sx={{ color: "white" }} aria-label="breadcrumb">
                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Home
                    </Typography>
                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      My account
                    </Typography>
                  </Breadcrumbs>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
      <Container>
        <Box className={classes.formPage}>
          <Grid container spacing={1}>
            <Grid item lg={6} md={6} xs={12}>
              <Typography
                className={classes.textFormPage}
                component="h4"
                variant="h4"
              >
                Login
              </Typography>
              <form
                onSubmit={handleSubmitLogin(fromSubmitLogin)}
                className={classes.formLogin}
                noValidate
              >
                <Typography sx={{ marginBottom: "15px", marginTop: "20px" }}>
                  Username or email address *
                </Typography>
                <InputForm
                  id="email"
                  lable="email"
                  type="email"
                  placeholder="Email"
                  register={{
                    ...registerLogin("email", {
                      validate: {
                        notAdmin: (field) => {
                          return (
                            field !== "admin@gmail.com" ||
                            "Enter a different email address"
                          );
                        },
                        notBlackListed: (field) => {
                          return (
                            !field.endsWith(".com") ||
                            " This is email not supported"
                          );
                        },
                      },
                    }),
                  }}
                  error={errorsLogin.email?.message}
                />
                <Typography sx={{ marginBottom: "15px", marginTop: "20px" }}>
                  Password *
                </Typography>
                <InputForm
                  id="password"
                  lable="Password"
                  type="password"
                  placeholder="Password"
                  register={{ ...registerLogin("password") }}
                  error={errorsLogin.password?.message}
                />

                <Button
                  color="primary"
                  variant="contained"
                  type="submit"
                  sx={{
                    marginTop: "20px",
                    borderRadius: "25px",
                    width: "100px",
                  }}
                >
                  Login
                </Button>
                <Box>
                  {/* <FormControlLabel
                    control={<Checkbox defaultChecked />}
                    label="Remember me"
                    checked={rememberMe}
                    onChange={() => setRememberMe(!rememberMe)}
                  /> */}
                  <Box>
                    <Typography component="span" variant="span">
                      <a href="#"> Lost your password?</a>
                    </Typography>
                  </Box>
                </Box>
              </form>
            </Grid>

            {/* Register */}
            <Grid item lg={6} md={6} xs={12}>
              <Box className={classes.regisPage}>
                <Typography
                  className={classes.textFormPage}
                  component="h4"
                  variant="h4"
                >
                  Register
                </Typography>
                <form
                  className={classes.formLogin}
                  onSubmit={handleSubmitRegister(fromSubmitRegister)}
                >
                  <Typography sx={{ marginBottom: "15px", marginTop: "20px" }}>
                    Username or email address *
                  </Typography>
                  <InputForm
                    id="email"
                    lable="Email"
                    type="email"
                    placeholder="Email"
                    register={{ ...registerRegister("email") }}
                    error={errorsRegister.email?.message}
                  />
                  <Typography sx={{ marginBottom: "15px", marginTop: "20px" }}>
                    Password *
                  </Typography>
                  <InputForm
                    id="password"
                    lable="Password"
                    type="password"
                    placeholder="Password"
                    register={{ ...registerRegister("password") }}
                    error={errorsRegister.password?.message}
                  />

                  <Button
                    color="primary"
                    variant="contained"
                    type="submit"
                    sx={{
                      marginTop: "20px",
                      borderRadius: "25px",
                      width: "100px",
                    }}
                  >
                    Register
                  </Button>
                </form>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
}
export async function getServerSideProps(context) {
  const { categories } = await getCategoriesData();

  const categorized = categories.filter((cate) => {
    const isUncategorizedCategory = cate.slug === "uncategorized";
    return !isUncategorizedCategory;
  });

  const usersLogin = await getLogin();

  return {
    props: {
      categorized,
      usersLogin,
    },
  };
}
