import { makeStyles } from "@material-ui/core/styles";
import Box from "@mui/material/Box";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import CartItemsContainer from "../components/cart/cart-page/CartItemsContainer";
import { getCategoriesData } from "../utils/categories";
import { getShippingData } from "../utils/shipping";

const useStyles = makeStyles({
  page: {
    marginBottom: "60px",
    minHeight: "200px",
    backgroundImage: `url("/banner_page.png)`,
    backgroundSize: "cover",
  },

  titlePage: {
    display: "flex",
    paddingTop: "75px",
    paddingBottom: "75px",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },

  rightTextPage: {
    paddingTop: "12px",
    float: "right",
    "@media (max-width: 768px)": {
      float: "inherit",
    },
  },
  textTile: {
    color: "white",
    fontFamily: "Merriweather",
    fontSize: "50px",
    fontWeight: 700,
  },
  titleText: {
    color: "white",
    fontFamily: "Muli",
    fontWeight: 400,
    fontSize: "14px",
  },
});

const Cart = (props) => {
  const { shipping } = props;
  const classes = useStyles();

  return (
    <>
      <Box className={classes.page}>
        <Container>
          <Box className={classes.titlePage}>
            <Grid container spacing={1}>
              <Grid item lg={6} md={6} xs={12}>
                <Box>
                  <Typography
                    className={classes.textTile}
                    component="h3"
                    variant="h3"
                  >
                    Cart
                  </Typography>
                </Box>
              </Grid>
              <Grid item lg={6} md={6} xs={12}>
                <Box className={classes.rightTextPage}>
                  <Breadcrumbs sx={{ color: "white" }} aria-label="breadcrumb">
                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Home
                    </Typography>

                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Cart
                    </Typography>
                  </Breadcrumbs>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
      <Container>
        <CartItemsContainer shipping={shipping} />
      </Container>
    </>
  );
};

export default Cart;
export async function getStaticProps() {
  const { shipping } = await getShippingData();

  const { categories } = await getCategoriesData();
  const categorized = categories.filter((cate) => {
    const isUncategorizedCategory = cate.slug === "uncategorized";
    return !isUncategorizedCategory;
  });

  return {
    props: {
      categorized,
      shipping,
    },
    revalidate: 1,
  };
}
