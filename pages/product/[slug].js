import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@mui/material";
import Box from "@mui/material/Box";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Container from "@mui/material/Container";
import Rating from "@mui/material/Rating";
import Typography from "@mui/material/Typography";
import { isEmpty } from "lodash";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import AddToCartButton from "../../components/cart/AddToCartButton";
import GalleryCarousel from "../../components/single-product/gallery-carousel";
import WooCommerce from "../../utils/apiWoocommerce";
import { getCategoriesData } from "../../utils/categories";
const colorHeading = "#323232";
const colorHover = "#40c6ff";
const colorDefaul = "#666";

const useStyle_productDetail = makeStyles({
  page: {
    marginBottom: "60px",
    minHeight: "200px",
    backgroundImage: `url("/banner_page.png")`,
    backgroundSize: "cover",
  },
  imgChi: {
    width: "100px",
    height: "100px",
    objectFit: "cover",
    border: "1px solid #40c6ff",
    marginRight: "5px",
    cursor: "pointer",
  },

  titlePage: {
    display: "flex",
    paddingTop: "75px",
    paddingBottom: "75px",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },

  rightTextPage: {
    paddingTop: "12px",
    float: "right",
    "@media (max-width: 768px)": {
      float: "inherit",
    },
  },

  imgagesDetail: {
    "& img": {
      width: "100%",
    },
  },
  galleryImages: {
    "& img": {
      width: "25%",
      border: "1px solid #ccc",
    },
  },
  name_product_detail: {
    fontSize: "30px",
    color: `${colorHeading}`,
    fontFamily: "Merriweather",
  },
  description_shot: {
    fontSize: "14px",
    color: "#000",
    textAlign: "justify",
    lineHeight: "23px",
  },
  product_meta: {
    marginTop: "30px",
    padding: "30px 0px",
    float: "left",
    width: "100%",
    marginBottom: "15px",
    borderTop: "1px solid #e4e4e4",
    borderBottom: "1px solid #e4e4e4",
    fontSize: "16px",
    color: "#676c77",
  },
  sku_title: {
    marginBottom: "15px",
  },
  description_detail: {
    marginTop: "70px",
    paddingTop: "50px",
    borderTop: "1px solid #e4e4e4",
    width: "100%",
    "& h4": {
      marginTop: "0px",
      fontSize: "20px",
      textTransform: "uppercase",
      fontFamily: "Merriweather",
    },
  },
  description_item: {
    fontSize: "16px",
    lineHeight: "23px",
    color: "#000",
    "& span": {
      fontSize: "16px",
      lineHeight: "23px",
      color: "#000",
    },
  },
  box_related_products: {
    width: "100%",
    marginTop: "30px",
    float: "left",
    margin: "0px -15px",
    "& h4": {
      fontSize: "20px",
      textTransform: "uppercase",
      textAlign: "center",
    },
  },
  related_products: {
    display: "inline-block",
    width: "25%",
    padding: "0px 15px",
    marginBottom: "30px",
    "& img": {
      width: "100%",
    },
    "@media (max-width:899px)": {
      width: "50%",
    },
    "@media (max-width:767px)": {
      width: "100%",
    },
  },
  box_product: {
    border: "1px solid #efefef",
    padding: "15px 15px 22px 15px",
    transition: "0.35s",
    "&:hover": {
      boxShadow: "0 10px 6px -6px #ccc",
    },
  },
  cardBody: {
    textAlign: "center",
    "& h3": {
      fontSize: "19px",
      fontWeight: "bold",
      color: "#444",
      marginTop: "12px",
      marginBottom: "10px",
      fontFamily: "Mulish,sans-serif",
      lineHeight: "24px",
      overflow: "hidden",
      display: "-webkit-box",
      "-webkit-box-orient": "vertical",
      "-webkit-line-clamp": "1",
    },
    "& h4": {
      marginTop: "6px",
      marginBottom: "0px",
    },
  },
  startRating: {
    textAlign: "center",
    "& span": {
      color: "#ffcc35",
      fontSize: "18px",
    },
  },
  cardPrice: {
    color: `${colorHover}`,
    fontSize: "22px",
    fontWeight: "500",
    fontFamily: "Mulish,sans-serif",
  },
  textTile: {
    color: "white",
    fontFamily: "Merriweather",
    fontSize: "50px",
    fontWeight: 700,
  },
  titleText: {
    color: "white",
    fontFamily: "Muli",
    fontWeight: 400,
    fontSize: "14px",
  },
  inputQuantity: {
    margin: " 0 10px",
    minWidth: "100px",
    outline: "none",
    border: " 1px solid rgba(0, 0, 0, 0.23)",
    padding: "10px",
  },
  regular_price: {
    color: "#CCCCCC",
    textDecoration: "line-through",
    fontSize: "20px",
    marginRight: "5px",
  },
  price: {
    color: "#40c6ff",
    fontSize: "30px",
    fontWeight: 600,
    marginRight: "5px",
  },
  priceTotal: {
    marginBottom: "20px",
  },
});

export default function Product({ product, relatedProducts }) {
  const classes = useStyle_productDetail();
  const [quantity, setQuantity] = useState(1);
  const [selectedImageUrl, setSelectedImageUrl] = useState(
    product?.images?.[0].src
  );

  const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value);
    if (!isNaN(newQuantity)) {
      setQuantity(newQuantity);
    }
  };
  const handleIncreaseQuantity = () => {
    const newQuantity = quantity + 1;
    setQuantity(newQuantity);
  };

  const handleDecreaseQuantity = () => {
    if (quantity > 0) {
      const newQuantity = quantity - 1;
      setQuantity(newQuantity);
    }
  };

  const handleImageClick = (imageUrl) => {
    setSelectedImageUrl(imageUrl);
  };

  return (
    <>
      <Box className={classes.page}>
        <Container>
          <Box className={classes.titlePage}>
            <Grid container spacing={1}>
              <Grid item lg={6} md={6} xs={12}>
                <Box>
                  <Typography
                    className={classes.textTile}
                    component="h3"
                    variant="h3"
                  >
                    Product Detail
                  </Typography>
                </Box>
              </Grid>
              <Grid item lg={6} md={6} xs={12}>
                <Box className={classes.rightTextPage}>
                  <Breadcrumbs sx={{ color: "white" }} aria-label="breadcrumb">
                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Home
                    </Typography>

                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Product Detail
                    </Typography>
                  </Breadcrumbs>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
      <div>
        {product ? (
          <div className={classes.singleProduct}>
            <Container maxWidth="lg">
              <Grid container direction="row" item lg={12} spacing={5}>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <div className={classes.imgagesDetail}>
                    <Image
                      src={selectedImageUrl}
                      alt="Image Details"
                      width={500}
                      height={500}
                    />
                    <div className={classes.galleryImages}>
                      {!isEmpty(product?.galleryImages?.nodes) ? (
                        <GalleryCarousel
                          gallery={product?.galleryImages?.nodes}
                        />
                      ) : !isEmpty(product.image) ? (
                        <Image
                          src={product?.image?.src}
                          alt="Product Image"
                          height="auto"
                          srcSet={product?.image?.srcSet}
                        />
                      ) : null}
                    </div>
                  </div>
                  {product.images.map((item, index) => {
                    return (
                      <Image
                        key={`index${index}`}
                        width={100}
                        height={100}
                        alt="Image"
                        src={item.src}
                        className={classes.imgChi}
                        onClick={() => handleImageClick(item.src)}
                      />
                    );
                  })}
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <div className="descDetail">
                    <h1 className={classes.name_product_detail}>
                      {product.name}
                    </h1>
                    <div className={classes.priceTotal}>
                      <span className={classes.regular_price}>
                        ${product?.regular_price}
                      </span>
                      <span className={classes.price}>${product?.price}</span>
                    </div>

                    <div
                      dangerouslySetInnerHTML={{
                        __html: product?.short_description,
                      }}
                      className={classes.description_shot}
                    />
                    <Button variant="outlined" onClick={handleDecreaseQuantity}>
                      -
                    </Button>
                    <input
                      className={classes.inputQuantity}
                      type="number"
                      min="1"
                      value={quantity}
                      onChange={handleQuantityChange}
                    />
                    <Button variant="outlined" onClick={handleIncreaseQuantity}>
                      +
                    </Button>
                    {<AddToCartButton product={product} quantity={quantity} />}

                    <div className={classes.product_meta}>
                      <div className={classes.sku_title}>
                        SKU : {product.sku}
                      </div>
                      <div className={classes.sku_title}>
                        Category : {product.name || product.slug}
                      </div>
                      <div className={classes.sku_title}>
                        Availability :{" "}
                        <Button variant="outlined">
                          {" "}
                          {product?.stock_status}
                        </Button>{" "}
                      </div>
                    </div>
                  </div>
                </Grid>
                <div className={classes.description_detail}>
                  <h4>Detail</h4>
                  <div
                    dangerouslySetInnerHTML={{ __html: product.description }}
                    className={classes.description_item}
                  ></div>
                </div>
                <div className={classes.box_related_products}>
                  <h4>Related products</h4>
                  {relatedProducts &&
                    relatedProducts.map((item, index) => (
                      <div
                        className={classes.related_products}
                        key={`index${index}`}
                      >
                        <div className={classes.box_product}>
                          <Link href={`/product/${item?.slug}`}>
                            <a>
                              <Image
                                width={250}
                                height={250}
                                src={item?.images[0]?.src}
                                alt={item?.name}
                                loading="lazy"
                              />
                            </a>
                          </Link>
                          <div className={classes.cardBody}>
                            <h3>
                              <Link href={`/product/${item?.slug}`}>
                                <a className={classes.cardTitle}>{item.name}</a>
                              </Link>
                            </h3>
                            <div className={classes.startRating}>
                              <Rating name="read-only" value={0} readOnly />
                            </div>
                            <h4 className={classes.cardPrice}>
                              <span className={classes.price_sales}>
                                ${item.price}
                              </span>
                            </h4>
                          </div>
                        </div>
                      </div>
                    ))}
                </div>
              </Grid>
            </Container>
          </div>
        ) : (
          ""
        )}
      </div>
    </>
  );
}

export async function getServerSideProps({ params }) {
  const { slug } = params;

  const productResponse = await WooCommerce.get(`products?slug=${slug}`);
  const product = productResponse.data[0];

  const relatedProductsResponse = await WooCommerce.get(
    `products?include=${product.related_ids.join(",")}`
  );
  const relatedProducts = relatedProductsResponse.data;
  const { categories } = await getCategoriesData();

  const categorized = categories.filter((cate) => {
    const isUncategorizedCategory = cate.slug === "uncategorized";
    return !isUncategorizedCategory;
  });
  return {
    props: {
      product: product,
      relatedProducts,
      categorized,
    },
  };
}
