import { Box, Container, Grid, Pagination } from "@mui/material";
import { makeStyles } from "@material-ui/core/styles";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import LazyLoad from "react-lazyload";
import LazyLoadIteams from "../components/LazyLoadIteams";
import SkeletonProduct from "../components/SkeletonProduct";
import NameForm from "../components/formEmail";
import SellerImageList from "../components/imglist";
import NewImageList from "../components/newlistimg";
import Product from "../components/product";
import SwipeableTextMobileStepper from "../components/slider";
import Tab from "../components/tab";
import TabSeller from "../components/tabSeller";
import Logo from "../components/tabsLogo";
import { getCategoriesData } from "../utils/categories";
import { getProductsData } from "../utils/product";
import DealsOfDay from "../components/DealsOfDay";
import { getBestSellers } from "../utils/bestSeller";

const useStyle_home = makeStyles({
  lazyLoad: {
    display: "flex",
    gap: "20px",
  },
  containerHome: {
    width: "100%",
    margin: "auto",
  },
  "@media (max-width: 768px)": {
    lazyLoad: {
      width: "100%",
    },
  },
  pagination: {
    display: "flex",
    justifyContent: "center",
    marginTop: "30px",
  },
});

const Home = ({
  products,
  categorized,
  productSellers,
  totalPages,
  totalProducts,
}) => {
  const router = useRouter();
  const classes = useStyle_home();
  const { category, sellerCategory } = router.query;
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    setFilteredProducts(products);
    setLoading(false);
  }, [category, sellerCategory, products]);

  // const sellers = productSellers.slice(0, 8)

  return (
    <>
      <Box>
        <SwipeableTextMobileStepper />
        <DealsOfDay />
        <Container>
          <Tab categorized={categorized} />
          <Grid
            container
            spacing={{ sm: 2, md: 2, xs: 4, lg: 4 }}
            columns={{ xs: 4, sm: 6, md: 4, lg: 4 }}
            className={classes.containerHome}
          >
            {loading ? (
              <SkeletonProduct />
            ) : (
              filteredProducts &&
              filteredProducts.map((product) => (
                <LazyLoad
                  height={100}
                  offset={100}
                  key={product.id}
                  placeholder={<LazyLoadIteams />}
                  className={classes.lazyLoad}
                >
                  <Product key={product.id} product={product} />
                </LazyLoad>
              ))
            )}
          </Grid>
        </Container>
        <NewImageList />
        <Container>
          <TabSeller categorized={categorized} />
          <Grid
            container
            spacing={{ sm: 2, md: 2, xs: 4, lg: 3 }}
            columns={{ xs: 4, sm: 6, md: 4, lg: 4 }}
          >
            {loading ? (
              <SkeletonProduct />
            ) : (
              productSellers &&
              productSellers.map((product) => (
                <LazyLoad
                  height={100}
                  offset={100}
                  key={product.id}
                  placeholder={<LazyLoadIteams />}
                  className={classes.lazyLoad}
                >
                  <Product key={product.id} product={product} />
                </LazyLoad>
              ))
            )}
          </Grid>
        </Container>
        <SellerImageList />
        <Logo />
        <NameForm />
      </Box>
    </>
  );
};

export default Home;

export async function getServerSideProps(context) {
  const { category, page, result, sellerCategory } = context.query;

  const searchValue = result || "";
  const { products, totalPages, totalProducts } = await getProductsData(
    11,
    page || 1,
    category,
    searchValue
  );
  const { categories } = await getCategoriesData();

  const categorized = categories.filter((cate) => {
    const isUncategorizedCategory = cate.slug === "uncategorized";
    return !isUncategorizedCategory;
  });

  const { productSellers } = await getBestSellers(8, sellerCategory);

  return {
    props: {
      products,
      totalPages,
      totalProducts,
      searchValue: searchValue,
      categorized,
      productSellers,
    },
  };
}
