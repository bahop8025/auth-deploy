import { yupResolver } from "@hookform/resolvers/yup";
import { makeStyles } from "@material-ui/core/styles";
import AddLocationIcon from "@mui/icons-material/AddLocation";
import EmailIcon from "@mui/icons-material/Email";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import Box from "@mui/material/Box";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import FormControl from "@mui/material/FormControl";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import * as React from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import * as yup from "yup";
import InputForm from "../components/checkout/InputForm";
import { sendContactForm } from "../libs/api";
import { getCategoriesData } from "../utils/categories";
import { getProductsData } from "../utils/product";
import {
  linksPageContactEmail,
  linksPageContactLocation,
  linksPageContactPhone,
} from "./api/linkPage";

const useStyles = makeStyles({
  pageAccout: {
    marginBottom: "60px",
    minHeight: "200px",
    background: `url("/banner_page.png")`,
    backgroundSize: "cover",
  },

  titlePage: {
    display: "flex",
    paddingTop: "75px",
    paddingBottom: "75px",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },
  textTile: {
    color: "white",
    fontFamily: "Merriweather",
    fontSize: "50px",
    fontWeight: 700,
  },
  titleText: {
    color: "white",
    fontFamily: "Muli",
    fontWeight: 400,
    fontSize: "14px",
  },
  rightTextPage: {
    paddingTop: "12px",
    float: "right",
    "@media (max-width: 768px)": {
      float: "inherit",
    },
  },
  inputAccout: {
    width: "100%",
    minHeight: "40px",
    paddingLeft: "10px",
    marginTop: "10px",
    marginBottom: "10px",
    border: "1px solid #ccc",
    outline: "none",
    borderRadius: "5px",
  },
  inputAccoutMasage: {
    width: "100%",
    height: "200px",
    paddingLeft: "10px",
    marginTop: "10px",
    marginBottom: "10px",
    border: "1px solid #ccc",
    outline: "none",
    borderRadius: "5px",
    "&:focus": {
      outline: "none",
    },
  },
  formPage: {
    display: "flex",
    padding: "30px",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },
  loginPage: {
    "@media (max-width: 768px)": {
      marginBottom: "40px",
    },
  },
  textFormPage: {
    marginBottom: "30px",
    textAlign: "left",
    fontFamily: "Merriweather",
    fontWeight: 700,
    fontStyle: "normal",
  },
  icon: {
    border: "1px solid",
    borderRadius: "40px",
    fontSize: "60px",
    padding: "15px",
    margin: "10px",
  },
  texticon: {
    paddingTop: "25px",
    fontSize: "19px",
    fontFamily: "Muli",
    fontWeight: 700,
    color: "#666666",
  },
});

const schema = yup.object({
  name: yup.string().required("Name is a required field"),
  email: yup.string().required().email("Email is not valid."),
  subject: yup.string().required("Subject is a required field"),
  message: yup.string().required("Message is a required field"),
});

export default function ContactUS() {
  const classes = useStyles();

  const {
    handleSubmit,
    register,
    formState: { errors },
    reset,
    control,
    watch,
  } = useForm({
    resolver: yupResolver(schema),
  });
  const { name, email, subject, message } = watch();

  const fromSubmit = async (data) => {
    await sendContactForm(data);
    reset();
    toast.success("Submitted successfully");
  };

  return (
    <>
      <Box className={classes.pageAccout}>
        <Container>
          <Box className={classes.titlePage}>
            <Grid container spacing={1}>
              <Grid item lg={6} md={6} xs={12}>
                <Box>
                  <Typography
                    className={classes.textTile}
                    component="h3"
                    variant="h3"
                  >
                    Contact
                  </Typography>
                </Box>
              </Grid>
              <Grid item lg={6} md={6} xs={12}>
                <Box className={classes.rightTextPage}>
                  <Breadcrumbs sx={{ color: "white" }} aria-label="breadcrumb">
                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Home
                    </Typography>
                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Contact
                    </Typography>
                  </Breadcrumbs>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
      <Container>
        <Box className={classes.formPage}>
          <Grid container spacing={1}>
            <Grid item lg={6} md={6} xs={12}>
              <Box className={classes.loginPage}>
                <Typography
                  className={classes.textFormPage}
                  component="h4"
                  variant="h4"
                >
                  Contact us
                </Typography>
                <Box>
                  <form onSubmit={handleSubmit(fromSubmit)}>
                    <FormControl sx={{ width: "100%", paddingRight: "40px" }}>
                      <InputForm
                        id="name"
                        lable="name"
                        type="text"
                        placeholder="lastName"
                        register={{ ...register("name") }}
                        error={errors.name?.message}
                        className={classes.inputAccout}
                      />
                      <InputForm
                        id="email"
                        lable="email"
                        type="email"
                        placeholder="Email"
                        register={{ ...register("email") }}
                        error={errors.email?.message}
                        className={classes.inputAccout}
                      />
                      <InputForm
                        id="subject"
                        lable="Subject"
                        type="text"
                        placeholder="Subject"
                        register={{ ...register("subject") }}
                        error={errors.subject?.message}
                        className={classes.inputAccout}
                      />
                      <InputForm
                        id="message"
                        lable="message"
                        type="text"
                        register={{ ...register("message") }}
                        error={errors.message?.message}
                        className={classes.inputAccoutMasage}
                        placeholder="Message"
                      ></InputForm>
                    </FormControl>
                    <Box>
                      <Button
                        disabled={!name || !email || !subject || !message}
                        type="submit"
                        variant="contained"
                        sx={{
                          marginTop: "20px",
                          borderRadius: "25px",
                          width: "50%",
                          color: "#fff ",
                        }}
                      >
                        SUBMIT NOW
                      </Button>
                    </Box>
                  </form>
                </Box>
              </Box>
            </Grid>
            <Grid item lg={6} md={6} xs={12}>
              <Box className={classes.regisPage}>
                <Typography
                  className={classes.textFormPage}
                  component="h4"
                  variant="h4"
                >
                  Location
                </Typography>
                <a href={linksPageContactPhone.href}>
                  <Box sx={{ display: "flex" }}>
                    <LocalPhoneIcon className={classes.icon} color="primary" />
                    <Typography
                      className={classes.texticon}
                      component="h6"
                      variant="h6"
                    >
                      {linksPageContactPhone.text}
                    </Typography>
                  </Box>
                </a>
                <a href={linksPageContactLocation.href}>
                  <Box sx={{ display: "flex" }}>
                    <AddLocationIcon className={classes.icon} color="primary" />
                    <Typography
                      className={classes.texticon}
                      component="h6"
                      variant="h6"
                    >
                      {linksPageContactLocation.text}
                    </Typography>
                  </Box>
                </a>
                <a href={linksPageContactEmail.href}>
                  <Box sx={{ display: "flex" }}>
                    <EmailIcon className={classes.icon} color="primary" />
                    <Typography
                      className={classes.texticon}
                      component="h6"
                      variant="h6"
                    >
                      {linksPageContactEmail.text}
                    </Typography>
                  </Box>
                </a>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
}

export async function getServerSideProps(context) {
  const { category, page, result } = context.query;

  const searchValue = result || "";
  const { products, totalPages, totalProducts } = await getProductsData(
    4,
    page || 1,
    category,
    searchValue
  );
  const { categories } = await getCategoriesData();
  const categorized = categories.filter((cate) => {
    const isUncategorizedCategory = cate.slug === "uncategorized";
    return !isUncategorizedCategory;
  });

  return {
    props: {
      products,
      totalPages,
      totalProducts,
      searchValue: searchValue,
      categorized,
    },
  };
}
