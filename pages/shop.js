import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { ListItemText, Pagination } from "@mui/material";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Container from "@mui/material/Container";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/system";
import { debounce } from "lodash";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import LazyLoad from "react-lazyload";
import LazyLoadIteams from "../components/LazyLoadIteams";
import SidebarTitle from "../components/SidebarTitle";
import FilterColor from "../components/filter/FilterColor";
import FilterRam from "../components/filter/FilterRam";
import FilterSelect from "../components/filter/FilterSelect";
import FilterSize from "../components/filter/FilterSize";
import FiterPrice from "../components/filter/FiterPrice";
import {
  FilterBy,
  FilterInputsOutputs,
  FilterResolution,
  FilterTVSize,
  FilterTvType,
} from "../components/filter/ItemsCheckbox";
import Product from "../components/product";
import {
  getAttributesAll
} from "../utils/attributes";
import { getCategoriesData } from "../utils/categories";
import { getProductsData } from "../utils/product";

const useStyles_pageShop = makeStyles((theme) => ({
  pageShop: {
    marginBottom: "60px",
    minHeight: "200px",
    backgroundImage: `url("/banner_page.png")`,
    backgroundSize: "cover",
  },

  titlePage: {
    display: "flex",
    paddingTop: "75px",
    paddingBottom: "75px",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },

  rightTextPage: {
    float: "right",
    paddingTop: "12px",
    "@media (max-width: 768px)": {
      float: "inherit",
    },
  },
  productCategory: {
    display: "flex",
    "@media (max-width: 768px)": {
      display: "flex",
      flexDirection: "column-reverse",
    },
  },
  textCartegory: {
    padding: "10px 0px",
    borderBottom: "1px solid #ccc",
    color: "#676c77",
    transition: "0.21s",
    "& div.MuiTreeItem-content": {
      padding: "0px!important",
      "&:hover": {
        background: "#fff",
      },
      "&:focus": {
        background: "#fff",
      },
      "& svg": {
        color: "#676c77",
      },
    },
    "&:hover": {
      color: "#000",
    },
  },
  titleCartegory: {
    marginBottom: "10px",
    marginTop: "10px",
  },
  categoryText: {
    textTransform: "uppercase",
    paddingLeft: "7px",
    "&:hover": {
      backgroundColor: "rgba(0, 0, 0, 0.12)",
      color: "#3498db",
    },
    "& span": {
      fontSize: "1rem",
    },
  },
  textTile: {
    color: "white",
    fontFamily: "Merriweather",
    fontSize: "50px",
    fontWeight: 700,
  },
  titleText: {
    color: "white",
    fontFamily: "Muli",
    fontWeight: 400,
    fontSize: "14px",
  },
  buttonShowAll: {
    width: "100%",
    height: "40px",
    border: "1px solid rgba(25, 118, 210, 0.5)",
    outline: "none",
    borderRadius: "5px",
    background: "transparent",
    cursor: "pointer",
    color: "#1976d2",
    fontSize: "14px",
  },
  noProduct: {
    width: "100%",
    height: "100%",
    textAlign: "center",
    color: "#40c6ff",
    fontSize: "30px",
    fontWeight: "bold",
  },
  results: {
    margin: "30px 0 30px 0",
  },
  pagination: {
    display: "flex",
    justifyContent: "center",
    marginTop: "30px",
  },
  gridProduct: {
    display: "flex",
    gap: "20px",
  },
  "@media (max-width: 768px)": {
    lazyLoadItem: {
      width: "100%",
    },
    gridProduct: {
      width: "100%",
      margin: "auto",
      paddingTop: "30px",
    },
    containerProduct: {
      paddingLeft: "0 !important",
    },
    productColumn: {
      display: "flex",
      flexDirection: "column-reverse",
      width: "100%",
      margin: "auto",
    },
    results: {
      margin: "0",
    },
  },
  "@media (min-width: 769px) and (max-width: 1024px)": {
    productColumn: {
      display: "flex",
      flexDirection: "column-reverse",
      width: "100%",
      margin: "auto",
    },
  },
}));

export default function Shop(props) {
  const router = useRouter();
  const previousUrlRef = useRef("");
  const classes = useStyles_pageShop();
  const {
    categorized,
    products,
    totalPages,
    totalProducts,
    attributes,
    attributesAll,
  } = props;
  const [currentPage, setCurrentPage] = useState(1);
  const [productList, setProductList] = useState(products);
  const [totalPage, setTotalPage] = useState(parseInt(totalPages));
  const [totalProduct, setTotalProduct] = useState();

  const colorProduct = attributesAll[1];
  const TvTypeTerm = attributesAll[5];
  const resolution = attributesAll[6];
  const inOut = attributesAll[7];
  const size = attributesAll[4];
  const filterBy = attributesAll[11];
  const ramProduct = attributesAll[2];
  const sizeProduct = attributesAll[9];

  const handlePushParameter = () => {
    return new URLSearchParams(router.query);
  };

  useEffect(() => {
    setProductList(products);
    setTotalPage(parseInt(totalPages));
    setTotalProduct(totalProducts);
  }, [products, parseInt(totalPages), totalProducts]);

  const handleChangeSize = (size) => {
    const searchParams = new URLSearchParams();
    const { id, slug_parent } = size;
    searchParams.set("id", id);
    searchParams.set("slug", slug_parent);
    const newUrl = `${router.pathname}?${searchParams.toString()}`;
    router.push(newUrl);
  };

  const handleChangeRam = (value) => {
    value.forEach((item) => {
      const newSearchParams = new URLSearchParams();
      const { id, slug } = item;
      newSearchParams.set("id", id);
      newSearchParams.set("slug", slug);
      const newUrl = `${router.pathname}?${newSearchParams.toString()}`;
      router.push(newUrl);
    });
  };

  const handleChangeSort = (orderby) => {
    const searchParams = handlePushParameter();
    searchParams.set("orderby", orderby);
    const newUrl = `${router.pathname}?${searchParams.toString()}`;
    router.push(newUrl);
  };

  const handleMinMaxPrice = debounce((valuePrice) => {
    const searchParams = handlePushParameter();
    searchParams.set("min_price", valuePrice[0]);
    searchParams.set("max_price", valuePrice[1]);
    const newUrl = `${router.pathname}?${searchParams.toString()}`;
    router.push(newUrl);
  }, 100);

  const handleCategory = (category) => {
    const searchParams = handlePushParameter();
    searchParams.set("category", category);
    const newUrl = `${router.pathname}?${searchParams.toString()}`;
    router.push(newUrl);
  };

  const handleCheckboxChange = (id, slug, checked) => {
    const newSearchParams = new URLSearchParams();
    if (checked) {
      newSearchParams.set("id", id);
      newSearchParams.set("slug", slug);
      newSearchParams.set(id, slug);
    } else {
      newSearchParams.delete("id", id);
      newSearchParams.delete("slug", slug);
      newSearchParams.delete(id, slug);
    }

    const currentPath = router.pathname;
    const newUrl = `${currentPath}?${newSearchParams.toString()}`;

    if (checked) {
      previousUrlRef.current = router.asPath; // Store the previous URL
      router.push(newUrl);
    } else {
      const previousUrl = previousUrlRef.current || currentPath;
      router.push(previousUrl);
    }
  };

  const handelResetUrl = () => {
    router.replace("/shop");
  };

  const handlePagination = async (event, page) => {
    event.preventDefault();
    const searchParams = handlePushParameter();
    searchParams.set("page", page);
    const newUrl = `${router.pathname}?${searchParams.toString()}`;
    router.push(newUrl);
    setCurrentPage(page);
  };

  return (
    <Box>
      <Box className={classes.pageShop}>
        <Container>
          <Box className={classes.titlePage}>
            <Grid container spacing={1}>
              <Grid item lg={6} md={6} xs={12}>
                <Box>
                  <Typography
                    className={classes.textTile}
                    component="h3"
                    variant="h3"
                  >
                    Shop
                  </Typography>
                </Box>
              </Grid>
              <Grid item lg={6} md={6} xs={12}>
                <Box className={classes.rightTextPage}>
                  <Breadcrumbs sx={{ color: "white" }} aria-label="breadcrumb">
                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Home
                    </Typography>
                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Shop
                    </Typography>
                  </Breadcrumbs>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
      <Container>
        <Box className={classes.productCategory}>
          <Grid container spacing={5} className={classes.productColumn}>
            <Grid item lg={3} className={classes.containerProduct}>
              <Box>
                <SidebarTitle title="Products Price Filter" />
                <FiterPrice onValueChange={handleMinMaxPrice} />
              </Box>
              <Box>
                <SidebarTitle title="Filter Size" />
                <FilterSize
                  attributes={attributes}
                  sizeProduct={sizeProduct}
                  onRadioChange={handleChangeSize}
                />
              </Box>
              <Box>
                <SidebarTitle title="Product Ram" />
                <FilterRam
                  attributes={attributes}
                  ramProduct={ramProduct}
                  onRadioChange={handleChangeRam}
                />
              </Box>

              <Box>
                <SidebarTitle title="Product Color" />
                <FilterColor
                  colorProduct={colorProduct}
                  attributes={attributes}
                  onRadioChange={handleCheckboxChange}
                />
              </Box>
              <Box>
                <SidebarTitle title="Product Category" />
              </Box>
              <List>
                <Divider />
                {categorized &&
                  categorized.map((category) => {
                    return (
                      <div key={category.id}>
                        <ListItem disablePadding>
                          <ListItemButton>
                            <ListItemText
                              key={category.id}
                              className={classes.categoryText}
                              onClick={() => handleCategory(category.id)}
                            >
                              {category.slug}
                            </ListItemText>
                          </ListItemButton>
                        </ListItem>
                        <Divider />
                      </div>
                    );
                  })}
              </List>
              <Box>
                <SidebarTitle title=" Product TV Screen Size" />
                <FilterTVSize
                  attributes={attributes}
                  size={size}
                  onRadioChange={handleCheckboxChange}
                />
              </Box>
              <Box>
                <SidebarTitle title=" Product Inputs/Outputs" />
                <FilterInputsOutputs
                  attributes={attributes}
                  inOut={inOut}
                  onRadioChange={handleCheckboxChange}
                />
              </Box>
              <Box>
                <SidebarTitle title="Product Resolution" />
                <FilterResolution
                  onRadioChange={handleCheckboxChange}
                  resolution={resolution}
                  attributes={attributes}
                />
              </Box>
              <Box>
                <SidebarTitle title="Product TV Type" />
                <FilterTvType
                  onRadioChange={handleCheckboxChange}
                  TvTypeTerm={TvTypeTerm}
                  attributes={attributes}
                />
              </Box>
              <Box>
                <SidebarTitle title="FilterBy" />
                <FilterBy
                  attributes={attributes}
                  filterBy={filterBy}
                  onRadioChange={handleCheckboxChange}
                />
              </Box>
              <Button variant="contained" onClick={handelResetUrl}>
                Reset
              </Button>
            </Grid>
            <Grid item lg={9} className={classes.containerProduct}>
              <Grid container spacing={6}>
                <Grid item xs={6}>
                  <Box>
                    <p className={classes.results}>
                      Showing all ({productList.length})/({totalProduct})
                      results
                    </p>
                  </Box>
                </Grid>
                <Grid item xs={6}>
                  <Box>
                    <FilterSelect onSortChange={handleChangeSort} />
                  </Box>
                </Grid>
              </Grid>

              <Grid
                container
                spacing={{ sm: 2, md: 2, xs: 3, lg: 3 }}
                columns={{ xl: 3, sm: 2, md: 3, lg: 3 }}
                className={classes.gridProduct}
              >
                {productList.length > 0 ? (
                  productList.map((product) => (
                    <LazyLoad
                      height={"100%"}
                      offset={100}
                      key={product.id}
                      className={classes.lazyLoadItem}
                      placeholder={<LazyLoadIteams />}
                    >
                      <Product key={product.id} product={product} />
                    </LazyLoad>
                  ))
                ) : (
                  <p className={classes.noProduct}>No Products</p>
                )}
              </Grid>
              <div className={classes.pagination}>
                <Pagination
                  color="primary"
                  count={totalPage}
                  page={currentPage}
                  onChange={handlePagination}
                />
              </div>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Box>
  );
}

export async function getServerSideProps(context) {
  const { category, page, id, slug, result, min_price, max_price, orderby } =
    context.query;
  const searchValue = result || "";

  const { categories } = await getCategoriesData();
  const categorized = categories.filter((cate) => {
    const isUncategorizedCategory = cate.slug === "uncategorized";
    return !isUncategorizedCategory;
  });

  const { products, totalPages, totalProducts } = await getProductsData(
    12,
    page || 1,
    category,
    searchValue,
    id,
    slug,
    min_price,
    max_price,
    orderby
  );
  const { attributesAll } = await getAttributesAll();

  return {
    props: {
      products,
      totalPages,
      totalProducts,
      searchValue: searchValue,
      categorized,
      attributesAll,
    },
  };
}
