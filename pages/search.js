import { makeStyles } from "@material-ui/core/styles";
import { Pagination } from "@mui/material";
import Box from "@mui/material/Box";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { useRouter } from "next/router";
import * as React from "react";
import { useEffect, useState } from "react";
import LazyLoad from "react-lazyload";
import LazyLoadIteams from "../components/LazyLoadIteams";
import Product from "../components/product";
import { getCategoriesData } from "../utils/categories";
import { getProductsData } from "../utils/product";
const colorHeading = "#323232";
const colorHover = "#40c6ff";
const colorDefaul = "#666";

const useStyles = makeStyles({
  pagination: {
    display: "flex",
    justifyContent: "center",
    marginTop: "30px",
  },
  pageAccout: {
    marginBottom: "60px",
    minHeight: "200px",
    background: `url("/banner_page.png")`,
    backgroundSize: "cover",
  },

  titlePage: {
    display: "flex",
    paddingTop: "75px",
    paddingBottom: "75px",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },
  textTile: {
    color: "white",
    fontFamily: "Merriweather",
    fontSize: "50px",
    fontWeight: 700,
  },
  titleText: {
    color: "white",
    fontFamily: "Muli",
    fontWeight: 400,
    fontSize: "14px",
  },
  rightTextPage: {
    paddingTop: "12px",
    float: "right",
    "@media (max-width: 768px)": {
      float: "inherit",
    },
  },
  inputAccout: {
    width: "100%",
    minHeight: "40px",
    paddingLeft: "10px",
    marginTop: "10px",
    marginBottom: "10px",
    border: "1px solid #ccc",
    outline: "none",
    borderRadius: "5px",
  },
  inputAccoutMasage: {
    width: "100%",
    height: "200px",
    paddingLeft: "10px",
    marginTop: "10px",
    marginBottom: "10px",
    border: "1px solid #ccc",
    outline: "none",
    borderRadius: "5px",
    "&:focus": {
      outline: "none",
    },
  },
  formPage: {
    display: "flex",
    padding: "30px",
    "@media (max-width: 768px)": {
      display: "block",
    },
  },
  loginPage: {
    "@media (max-width: 768px)": {
      marginBottom: "40px",
    },
  },
  textFormPage: {
    marginBottom: "30px",
    textAlign: "left",
    fontFamily: "Merriweather",
    fontWeight: 700,
    fontStyle: "normal",
  },
  icon: {
    border: "1px solid",
    borderRadius: "40px",
    fontSize: "60px",
    padding: "15px",
    margin: "10px",
  },
  texticon: {
    paddingTop: "25px",
    fontSize: "19px",
    fontFamily: "Muli",
    fontWeight: 700,
    color: "#666666",
  },
  itemProduct: {
    marginBottom: 20,
  },
  cardButton: {
    paddingBottom: 15,
  },
  productImg: {
    width: "100%",
    height: "auto",
    display: "inline-block",
    textAlign: "center",
  },
  cardBody: {
    textAlign: "center",
    "& h3": {
      fontSize: "19px",
      fontWeight: "bold",
      color: "#444",
      marginTop: "12px",
      marginBottom: "10px",
      fontFamily: "Mulish,sans-serif",
    },
    "& h4": {
      marginTop: "6px",
      marginBottom: "0px",
    },
  },
  cardTitle: {
    "&:hover": {
      color: `${colorHover}`,
    },
  },
  startRating: {
    "& span": {
      color: "#ffcc35",
      fontSize: "18px",
    },
  },
  cardPrice: {
    color: `${colorHover}`,
    fontSize: "22px",
    fontWeight: "500",
    fontFamily: "Mulish,sans-serif",
  },
  regular_price: {
    textDecoration: "line-through",
    marginRight: "15px",
    color: "#999",
    fontWeight: "400",
  },
  box_product: {
    border: "1px solid #efefef",
    padding: "15px 15px 22px 15px",
    transition: "0.35s",
    "&:hover": {
      boxShadow: "0 10px 6px -6px #ccc",
    },
  },
  lazyLoad: {
    display: "flex",
    gap: "20px",
  },
  containerSearch: {
    width: "100%",
    margin: "auto",
  },
  "@media (max-width: 768px)": {
    lazyLoad: {
      width: "100%",
    },
  },
});

export default function Search({
  products,
  totalProducts,
  totalPages,
  searchValue,
}) {
  const classes = useStyles();
  const router = useRouter();
  const [totalPage, setTotalPage] = useState(parseInt(totalPages));
  const [currentPage, setCurrentPage] = useState(1);
  const [productList, setProductList] = useState(products);
  const [totalProduct, setTotalProduct] = useState();

  const handlePushParameter = () => {
    return new URLSearchParams(router.query);
  };

  useEffect(() => {
    setProductList(products);
    setTotalPage(parseInt(totalPages));
    setTotalProduct(totalProducts);
  }, [products, parseInt(totalPages), totalProducts]);

  const handlePagination = async (event, page) => {
    event.preventDefault();
    const searchParams = handlePushParameter();
    searchParams.set("page ", page);
    const newUrl = `${router.pathname}?${searchParams.toString()}`;
    router.push(newUrl);
    setCurrentPage(page);
  };

  const handleSearch = () => {
    if (searchValue.trim() === "") {
      return (
        <Typography variant="h5">
          There&apos;s no product you&apos;re looking for....
        </Typography>
      );
    } else if (products.length > 0) {
      return (
        <>
          <Typography variant="h5" sx={{ marginBottom: "30px" }}>
            Search Results for: {searchValue}
          </Typography>
          <Grid
            container={true}
            spacing={{ sm: 2, md: 2, xs: 4, lg: 2 }}
            columns={{ xs: 4, sm: 6, md: 4, lg: 4 }}
            className={classes.containerSearch}
          >
            {products.map((product) => {
              return (
                <LazyLoad
                  height={100}
                  offset={100}
                  key={product.id}
                  placeholder={<LazyLoadIteams />}
                  className={classes.lazyLoad}
                >
                  <Product key={product.id} product={product} />
                </LazyLoad>
              );
            })}
          </Grid>
          <div className={classes.pagination}>
            <Pagination
              color="primary"
              count={totalPage}
              page={currentPage}
              onChange={handlePagination}
            />
          </div>
        </>
      );
    } else {
      return (
        <Typography variant="h5">
          No products found for: {searchValue}
        </Typography>
      );
    }
  };

  return (
    <>
      <Box className={classes.pageAccout}>
        <Container container>
          <Box className={classes.titlePage}>
            <Grid container={true} spacing={1}>
              <Grid item lg={6} md={6} xs={12}>
                <Box>
                  <Typography
                    className={classes.textTile}
                    component="h3"
                    variant="h3"
                  >
                    Search
                  </Typography>
                </Box>
              </Grid>
              <Grid item lg={6} md={6} xs={12}>
                <Box className={classes.rightTextPage}>
                  <Breadcrumbs sx={{ color: "white" }} aria-label="breadcrumb">
                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Home
                    </Typography>
                    <Typography
                      className={classes.titleText}
                      component="h6"
                      variant="h6"
                    >
                      Search
                    </Typography>
                  </Breadcrumbs>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
      <Container>{handleSearch()}</Container>
    </>
  );
}
export async function getServerSideProps(context) {
  const { category, page, resultSearch } = context.query;

  const searchValue = resultSearch || "";
  const { categories } = await getCategoriesData();
  const categorized = categories.filter((cate) => {
    const isUncategorizedCategory = cate.slug === "uncategorized";
    return !isUncategorizedCategory;
  });

  const { products, totalPages, totalProducts } = await getProductsData(
    12,
    page || 1,
    category,
    searchValue
  );
  return {
    props: {
      products,
      totalPages,
      totalProducts,
      searchValue: searchValue,
      categorized,
    },
  };
}
